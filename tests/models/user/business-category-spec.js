describe("Validations on business signup page",function(){
	it("Verify that clicking submit button after entering all the required 	fields, submits the data to the server ",function(){
		expect(true).toBeTruthy();
	});

	it("Verify that not filling the optional fields and clicking submit button 	will still send data to server without any validation error",function(){
		expect(true).toBeTruthy();
	});


	it("Check validation email fields (only valid email Ids should be allowed)",function(){
		expect(true).toBeTruthy();
	});


	it("Verify that after making a request to the server and then sending 	the same request again with the same unique key will lead to server side validation error ",function(){
		expect(true).toBeTruthy();
	});
});	