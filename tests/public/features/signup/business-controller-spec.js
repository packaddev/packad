/**
 * 
 *   Home controller spec
 *
 *   This is to test home-controller used for the Business Signup page.
 *   It has functionalities for logout,email verification & dropdowns in business signup page
 * 
 */

describe('Business Controller spec',function(){
  beforeEach(module('signUp'));

  var $controller;

  beforeEach(angular.mock.inject(function(_$controller_){
    $controller = _$controller_;
  })

  describe('List of countries displayed on Business signup page',function(){
      it('All the countries should be loaded',function()
      {
        var $scope = {};
        var controller = $controller('BusinessController', { $scope: $scope });
        var countries = commonDataService.getCountries();
        expect(countries).toBeRequired();
      });
    });

  describe('List of states displayed on Business signup page',function(){
      it('All the states should be loaded',function()
      {
        var $scope = {};
        var controller = $controller('BusinessController', { $scope: $scope });
        var states = commonDataService.getStates();
        expect(states).toBeRequired();
      });
    });


  describe('List of Business Categories displayed on Business signup page',function(){
      it('All the Business Categories should be loaded',function()
      {
        var $scope = {};
        var controller = $controller('BusinessController', { $scope: $scope });
        var BusinessCategories = commonDataService.getBusinessCategories();
        expect(BusinessCategories).toBeRequired();
      });
    });

  describe('Registering a user with all the details entered',function(){
      it('User should be saved on DB',function()
      {
        var ct = this;
        ct.image = ct.image || undefined;
        ct.user.makeSave(ct.image);
        expect(ct.image).toBeExpected();
      });
    });
);