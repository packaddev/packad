angular.module('packAdApp')
.factory('commonDataService', function($http) {
    var factory = {};

    factory.getCountries = function() {
                    		return $http.get('./data/countryList.json');
        			        }

    factory.getStates = function() {
                    		return $http.get('./data/stateList.json');
        			        }

    factory.getBusinessCategories = function() {
                    		return $http.get('./data/businessCategories.json');
        			        }

    return factory;
});