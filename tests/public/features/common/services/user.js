/**
 * 
 *   Pack add User service
 *
 *   This file exports a class that can be used for all the operations
 *   on user
 *   
 */

angular.module('packAdApp')
  .factory('paUser', ['$resource', 'APISERVER', '$state', 'Upload', '$rootScope', function(resource, apiRoute, state, Uploader, rootScope) {

    /**
     * url - default route for all the user specific operations
     */
    var url = apiRoute + 'users/:id';
    var uploaderUrl = apiRoute + 'upload/user/';

    /**
     * transformRequest - Method for transforming request object 
     *                    before sending to http 
     * @param  {Object} data - data sent to resource method
     * @return {Object}      data sent to http method
     */
    var transformRequest = function(request) {
      // operations on data being sent to http
      return angular.toJson(request);
    };

    /**
     * transformResponse - Method for transdorming the response 
     *                     recieved from http
     * @param  {Object} result - data recieved from http
     * @return {Object}        -  data being sent in the promise
     */
    var transformResponse = function(data) {
      // operations on response from http
      return angular.fromJson(data);
    };

    /**
     * paUser - Pack ad user object 
     *          This object is the class that we expose as a dependency
     * @type {Object}
     */
    var paUser = resource(url, { 'id': '@_id' }, {
      save: {
        method: 'POST',
        isArray: false,
        transformRequest: transformRequest,
        transformResponse: transformResponse
      },
      query: {
        method: 'GET',
        params: {},
        isArray: true
      },
      get: {
        method: 'GET',
        isArray: false,
        transformResponse: transformResponse
      },
      update: {
        method: 'POST',
        isArray: false,
        transformRequest: transformRequest,
        transformResponse: transformResponse
      }
    });

    /**
     * makeSave - Adding methods to the class that will be accessable
     *            when we create a new user
     */
    paUser.prototype.makeSave = function(image) {
      var operation;
      if (this._id) {
        operation = this.$update();
      } else {
        operation = this.$save();
      }

      /**
       * operation is a promise.
       *
       * .then contains two functions
       * 1st is success handler 
       * 2nd is error handler
       */
      operation.then(function(data) {
        /**
         * If user has uploaded the image then save the uploaded image
         * else move to home page
         */
        if (image) {
          // Uploader.upload({
          //   url: uploaderUrl + data._id,
          //   data: { file: image }
          // }).then(function(resp) { //upload function returns a promise
          //   if (resp.data.error_code === 0) { //validate success
          //     console.log('Image uploaded Successfully. File name: ' + resp.config.data.file.name);
          //     rootScope.currentUser = data;
          //     state.go('home');
          //   } else {
          //     console.log('Error uploading image: ' + resp.status);
          //   }
          // }, function(resp) { //catch error
          //   console.log('Error status: ' + resp.status);
          // });
          rootScope.avatar = image;
          console.log(image);
          console.log(image.fullPath);
          rootScope.currentUser = data;
          state.go('home');
        } else {
          /**
           * if user did not upload image move to home page
           */
          rootScope.currentUser = data;
          state.go('home');
        }
      }, function(err) {
        // We could show model window here
        console.log(err);
      });
    };

    return paUser;
  }]);
