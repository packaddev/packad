/**
 * 
 *   Pack add User service
 *
 *   This is to know the data being loaded from the Json's
 *  
 *   
 */

describe('Common Data Service spec',function(){

  describe('Countries List Json',function()
  {
    it("Countries list should be as expected from Static Json",function()
    {
      expect(3).toBe(3);
    });
  });
  
  describe('States List Json',function()
  {
    it("States list should be as expected from Static Json",function()
    {
      expect(3).toBe(3);
    });
  });
  
  describe('Categories List Json',function()
  {
    it("Categories list should be as expected from Static Json",function()
    {
      expect(3).toBe(3);
    });
  });

  describe('Sub-Categories List Json',function()
  {
    it("Sub-Categories list should be as expected from Static Json",function()
    {
      expect(3).toBe(3);
    });
  });


});
