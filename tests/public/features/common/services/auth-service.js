angular.module('packAdApp')
  .factory('AuthService', function($http) {
    var authService = {};

    authService.login = function(credentials) {
      return $http
        .post('/login', credentials)
        .then(function(res) {
          console.log(res.data);
          var user = {
            name: res.data.name,
            token: res.data.token,
            role: res.data.role,
            success: res.data.success,
            status: res.data.status,
            email: res.data.email
          };
          return user;
        });
    };

    authService.isAuthenticated = function() {
      return !!rootScope.user.email;
    };

    authService.isAuthorized = function(authorizedRoles) {
      if (!angular.isArray(authorizedRoles)) {
        authorizedRoles = [authorizedRoles];
      }
      return (authService.isAuthenticated() &&
        authorizedRoles.indexOf(rootScope.user.role) !== -1);
    };

    return authService;
  });
