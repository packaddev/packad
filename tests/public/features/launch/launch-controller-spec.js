/**
 *   Launch controller spec
 *   This is to test launch-controller used for the default page.
 *   It has functionalities for logging a user 
 *   redirection of a user to sign up page, etc,.
*/


describe('Launch Controller spec', function () {
  beforeEach(module('packAdApp'));
    var $controller;
  beforeEach(angular.mock.inject(function (_$controller_) {
        $controller = _$controller_;
    }));

    var credentials = {
        email: 'ajay@packad.com',
        password: 'Packadtest@123'
    };

    console.log(credentials);

    describe('Login', function () {
      it('Verifying whether login is successful', function () {
            var controller = $controller('LaunchController', credentials, '', '', '', '');
            var verifylogin = controller.login(credentials);
            expect(verifylogin).toBeTruthy();
        });
    });
});