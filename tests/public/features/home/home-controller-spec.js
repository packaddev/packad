/**
 * 
 *   Home controller spec
 *
 *   This is to test home-controller used for the Business Signup page.
 *   It has functionalities for logout,email verification & dropdowns in business signup page
 * 
 */

describe('Home Controller spec',function(){
  beforeEach(module('packAdApp'));

  var $controller;

  beforeEach(angular.mock.inject(function(_$controller_){
    $controller = _$controller_;
  });


    describe('SubCategory Details on GroupBox',function(){
      it('Verifying SubCategory on GroupBox',function()
      {
        var $scope = {};
        var controller = $controller('HomeController', { $scope: $scope });
        var SubCategory = $scope.SubCategoryList();
        expect(SubCategory).toBeTruthy();
      });
    });

    describe('Logout',function(){
      it('Verifying whether logout is successfull',function()
      {
        var $scope = {};
        var controller = $controller('HomeController', { $scope: $scope });
        var verifylogout = $scope.logout('ajay@packad.com');
        expect(verifylogout).toBeTruthy();
      });
    });

    describe('CategoryList',function(){
      it('Verifying category on GroupBox',function()
      {
        var $scope = {};
        var controller = $controller('HomeController', { $scope: $scope });
        var Category = $scope.CategoryList();
        expect(Category).toBeTruthy();
      });
    });
});
