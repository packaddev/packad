// Karma configuration
// Generated on Sat Jul 09 2016 16:26:42 GMT+0530 (India Standard Time)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',

	 plugins : [
		'karma-htmlfile-reporter',
		'karma-jasmine',
		'karma-chrome-launcher',
        'karma-coverage',
        'karma-mocha',
        'karma-chai'
	],

    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine','mocha','chai'],

    // list of files / patterns to load in the browser
    files: [
   
      //Bower dependencies
          './application/public/bower_components/jquery/dist/jquery.min.js'
          './application/public/bower_components/bootstrap/dist/js/bootstrap.min.js'
          './application/public/bower_components/angular/angular.min.js'
          './application/public/bower_components/angular-animate/angular-animate.min.js'
          './application/public/bower_components/angular-bootstrap/ui-bootstrap.min.js'
          './application/public/bower_components/angular-resource/angular-resource.min.js'
          './application/public/bower_components/angular-ui-router/release/angular-ui-router.min.js'
          './application/public/bower_components/ng-file-upload/ng-file-upload.min.js'
          './application/public/bower_components/ng-file-upload/ng-file-upload-shim.min.js'

      //modules
          './application/public/features/signup/signup-module.js'

      //shared services
          './application/public/features/common/services/user.js'
          './application/public/features/common/services/commonDataService.js'
          './application/public/features/common/services/auth-service.js'
          './application/public/features/alert/alert-service.js'

      //directives
          './application/public/features/home/directives/groupbox-directive.js'

      //controllers
          './application/public/features/launch/launch-controller.js'
          './application/public/features/home/home-controller.js'
          './application/public/features/signup/business-controller.js'

      //specs
          //'tests/**/**/*-spec.js'
          './tests/public/features/launch/launch-controller-spec.js'
    ],
	
    // list of files to exclude
    exclude: [
    ],

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {'tests/**/*-spec.js':['coverage']
    },

    coverageReporter : {
      type : 'html',
      dir : 'tests/coverage/'
    },

    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress','html','coverage'],

	htmlReporter: {
            outputFile: 'tests/unit test result.html'
        },

    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
