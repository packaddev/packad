'use strict';

/*
 *  Sample URL filter to log all the incoming requests to console
 *  For more information on Middleware visit: http://expressjs.com/api.html#middleware
 */
module.exports = function(req, res, next) {
  console.log('\n\n The URL hit :: ' + req.url);
  next();
};
