var errorUtils = require('../lib/err-util.js');

module.exports = function(req, res, next) {
  /**
   * Check if request session has the same as req headers
   *
   * if session is not present then send back error
   */
  if (req.session['sessionKey'] === req.get('authKey')) {
    console.log('User Authorization.. !!');
    next();
  } else {
    return errorUtils.handleCustomError(res, 412, {
      'status': 'fail',
      'message': 'Session Expired/deleted try again later'
    });
  }
}
