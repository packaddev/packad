'use strict';

var express = require('express');
var kraken = require('kraken-js');
var emailService = require('../controllers/email.js');
var mailConstants = require('../templates/email-constants.js');
var utilities = require('../lib/util.js');
var auth = require('../middlewares/auth.js');

var api = {
  user: require('../controllers/api/user.js'),
  app: require('../controllers/app.js'),
  email: require('../controllers/email.js'),
  uploader: require('../controllers/api/uploader.js'),
  groupBox: require('../controllers/api/groupbox.js'),
  ad: require('../controllers/api/ad.js')
}

var router = express.Router();

/**
 * Launch and login related
 */
router.route('/').get(api.app.get);
/**
 * user related operations
 */
//Route to log in the user
router.route('/api/users/login').post(api.user.logIn);
//Route to logout a user
router.route('/api/users/logout').get(api.user.logOut);
//Route to check of email id is already existing
router.route('/api/users/checkEmailExist').post(api.user.checkEmailExist);
//Route to send email verification
router.route('/api/users/sendEmailVerification').post(api.user.sendEmailVerification);
//Route to fetch all users
router.route('/api/users').get(api.user.getAll);
//Route to create a user
router.route('/api/users').post(api.user.create);
//Route to get only one user
router.route('/api/users/:id').get(api.user.getOne);
//Route to update a user information
router.route('/api/users/:id').post(auth, api.user.update);

router.route('/api/users/emailConfirmation/:verstr').get(api.user.emailConfirmation);
//Route to upload image of a user during registration process
router.route('/api/upload/user/:id').post(api.uploader.user);
//Route to save user goup box details
router.route('/api/groupBox/connect').post(api.groupBox.connect);
//Route to delete user goup box details
router.route('/api/groupBox/disconnect').post(api.groupBox.disconnect);
//Route to save user goup box details
router.route('/api/groupBox/getAll/:id').get(api.groupBox.getAll);


/**
 * Ad related operations
 */
// create Ad
router.route('/api/ad').post(api.ad.create);

module.exports = router;
