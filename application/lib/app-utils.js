/**
 *
 *    This file is only used for application specific utility functions
 *    For example:
 *      Accessing defaults,
 *      Getting environment Variables etc
 *
 *    This is different from util.js
 *      Util.js is only used to have general method that will be used
 *      across the application like date formats etc.
 * 
 */


/**
 * appConfig   - Global variable to this file only.
 *               Used to hold the values of all the application specific defaults
 */
var appConfig;
/**
 * wholeConfig - Global variable to this file only.
 *               Used to hold the values of all the kraken specific defaults
 */
var wholeConfig;


module.exports = {
  /**
   * setConfig - used to set the configurations in a variable that is accessable as a utility.
   * @param {Object}  config - The whole configuration object formed after kraken is called.
   *                           We set the wholeConfig and appConfig variables here.
   *
   * NOTE :: This method shouldnot be called anywhere in the controller!!
   *         This method is purely used to set the configuration object on the run time when
   *         kraken is initiated.
   */
  setConfig: function(config) {
    /**
     * Avoiding any enhancements to appConfig and wholeConfig as they have to be set only once
     */
    if (!appConfig && !wholeConfig) {
      appConfig = config ? (config._store ? config._store.application : undefined) : undefined;
      wholeConfig = config ? config._store : undefined;
    } else {
      return;
    }
  },
  /**
   * getConfig - Used to get the config file whenever/where ever needed
   *             As of now returning only the application specific defaults object.
   *             
   *             We can also have thsi function return differenct configuration objects based
   *             on parameters passed into it.
   * @return {Object} appConfig - application specific configuration object
   */
  getConfig: function() {
    /**
     * Return application configuration only if it is defined
     */
    if (appConfig) {
      return appConfig;
    } else {
      console.log('Application config not found');
    }
  },
  /**
   * getEnvironment - Method used to get the environment currently working in.
   *                  Response will always be in UPPER CASE. This must be kept in mind while checking for it.
   * @return {String} - Environment currently working in
   */
  getEnvironment: function() {
    /**
     * Return enviroment only if application has started
     */
    if (wholeConfig) {
      return wholeConfig.env.env.toUpperCase();
    } else {
      return 'Not yet configured'
    }
  }
};
