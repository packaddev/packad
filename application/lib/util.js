var crypto = require('crypto');
var config = require('../../config/config.js');
var appUtils = require('./app-utils.js');

var utilities = {
  encrypt: function(text) {
    var cipher = crypto.createCipher(config.cryptoalgorithm, config.cryptopassword)
    var crypted = cipher.update(text, 'utf8', 'hex')
    crypted += cipher.final('hex');
    return crypted;
  },

  decrypt: function(text) {
    var decipher = crypto.createDecipher(config.cryptoalgorithm, config.cryptopassword)
    var dec = decipher.update(text, 'hex', 'utf8')
    dec += decipher.final('utf8');
    return dec;
  },

  createSessionKey: function() {
    /**
     * The arguments provided by js is special type of object so forming a seperate array 
     * that holds arguments that are passed to function and then following operations follow.
     * 
     * Iterateing over the arguments provided randomly and then providing insetring
     * chiper text between them.
     *
     * This has to be encrypted again.
     */


    /**
     * args - Holds the arguments passed to function
     * @type [String]
     */
    var args = [];

    for (var i = 0; i < arguments.length; i++) {
      args.push(arguments[i]);
    }

    /**
     * currentIndex - index of element that is being shuffled
     * @type {Number}
     */
    var currentIndex = args.length,
      temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = args[currentIndex];
      args[currentIndex] = args[randomIndex];
      args[randomIndex] = (typeof(temporaryValue) === 'string') ? temporaryValue.toString().split(' ').join('') : temporaryValue;
    }

    /**
     * Now the array is shuffled
     * Join the arguments with chiper text and then encrypt it further
     *
     * encryptoin is pending as of now. sending joinied string for now
     */
    return args.join(appUtils.getConfig().userSessionChiper);

  }
}

module.exports = utilities;
