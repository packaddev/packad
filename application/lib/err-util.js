/**
 *  This file implements common error handling functions, used across all node controlellers
 */

'use strict';

module.exports = {
  handleError: function(res, errMessage) {
    errMessage = (typeof errMessage === 'object') ? JSON.stringify(errMessage) : errMessage;
    console.log('\nFatal Error in Server :: ' + errMessage);
    console.log('\nReturning 500 HTTP error to client');
    return res.status(500).send(errMessage);
  },
  handleCustomError: function(res, errCode, errMessage) {
    errMessage = (typeof errMessage === 'object') ? JSON.stringify(errMessage) : errMessage;
    console.log('\nApplication Error in Server :: ' + errMessage);
    console.log('\nReturning ' + errCode + ' HTTP error to client');
    return res.status(errCode).send(errMessage);
  }
};
