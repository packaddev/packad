/**
 *
 *    This file is used to connect to DB and
 *    create classes that can be used to make DB operations
 *
 *    Global variables created are the classes that will be used.
 *
 */


var mongoose = require('mongoose'),
  dbError = 'Looks like there is error conneting to DB',
  User, UserRole, UserStatus, Address, UserGroupBox, GroupBox, Ad;

/**
 * We export methods that return class object (Schema constructor function)
 *
 * Note that these classes will be created when we connect to Database
 */
module.exports = {
  connect: function(url, options, callBack) {
    console.log('\n db.connect :: method entry');
    mongoose.connect(url, options, function(err, success) {
      if (err) {
        console.log('\n db.connect :: db connection error');
        console.log(dbError);
        return dbError;
      }

      /**
       * On successfull connection to data base we create classes
       * by taking the schema that we have created
       */
      var userRole = require('../models/user/user-role.js');
      UserRole = mongoose.model('userRole', userRole);

      var userStatus = require('../models/user/user-status.js');
      UserStatus = mongoose.model('userStatus', userStatus);

      var user = require('../models/user/user.js');
      User = mongoose.model('user', user);

      var address = require('../models/user/address.js');
      Address = mongoose.model('address', address);

      var userGroupBox = require('../models/user/user-group-box.js');
      UserGroupBox = mongoose.model('userGroupBox', userGroupBox);

      var groupBox = require('../models/user/group-box.js');
      GroupBox = mongoose.model('groupBox', groupBox);

      var ad = require('../models/ad/ad.js');
      Ad = mongoose.model('ad', ad);


      console.log('connected to db : ' + url);
      callBack();
    });
  },

  disconnect: function() {
    mongoose.connection.close();
  },
  /**
   * getUserClass - method to return user class
   */
  getUserClass: function() {
    if (User) {
      return User;
    } else {
      return dbError;
    }
  },
  /**
   * getUserRoleClass - method to return user role class
   */
  getUserRoleClass: function() {
    if (UserRole) {
      return UserRole;
    } else {
      return dbError;
    }
  },
  /**
   * getUserStatusClass - method to return user status class
   */
  getUserStatusClass: function() {
    if (UserStatus) {
      return UserStatus;
    } else {
      return dbError;
    }
  },
  /**
   * getAdClass - method to return Ad class
   */
  getAdClass: function() {
    if (Ad) {
      return Ad;
    } else {
      return dbError;
    }
  },

  getAddressClass: function() {
    if (Address) {
      return Address;
    } else {
      return dbError;
    }
  },
  getUserGroupBoxClass: function() {
    if (UserGroupBox) {
      return UserGroupBox;
    } else {
      return dbError;
    }
  },
  getGroupBoxClass: function() {
    if (GroupBox) {
      return GroupBox;
    } else {
      return dbError;
    }
  }


}
