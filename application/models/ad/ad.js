var mongoose = require('mongoose');
var serviceDetails = require('./service-details.js');
// var address = require('./address.js');

module.exports = mongoose.Schema({
  businessCategoryId: { type: Number, required: true },
  businessSubCategoryId: { type: Number, required: true },
  service: { type: String, required: true },
  serviceDetails: typeof serviceDetails
}, { collection: 'ad', strict: true });
