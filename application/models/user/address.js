var mongoose = require('mongoose');

module.exports = mongoose.Schema({
  street: { type: String, required: false },
  city: { type: String, required: false },
  stateId: { type: String, required: false },
  countryId: { type: String, required: false },
  zipcode: { type: Number, required: false }
});
