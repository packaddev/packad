var mongoose = require('mongoose');

module.exports = mongoose.Schema({
  businessCategoryId: { type: Number, required: false },
  businessSubCategoryId: { type: String, required: false },
  isActive: { type: Boolean, required: true },
  boxNumber: { type: Number, required: false }
}, { collection: 'groupBox', strict: true });
