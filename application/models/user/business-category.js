/**
 * Business category model
 * @type {[type]}
 */
var mongoose = require('mongoose');

module.exports = mongoose.Schema({
  categoryId: { type: Number, required: true },
  categoryDesc: { type: String, required: true }
}, { collection: 'categories', strict: false });
