/**
 * exports user object
 * @type {[type]}
 */
var mongoose = require('mongoose');
var userRole = require('./user-role.js');
var userStatus = require('./user-status.js');
var businessCategory = require('./business-category.js');
var address = require('./address.js');

module.exports = mongoose.Schema({
  firstName: { type: String, required: false, unique: false },
  lastName: { type: String, required: false, unique: false },
  middleName: { type: String, required: false, unique: false },
  fullName: { type: String, required: false, unique: false },
  genderId:{type: String, required: false, unique: false},
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  companyName: { type: String, required: false },
  businessType: typeof businessCategory,
  address: typeof address,
  website: { type: String, required: false },
  profilePicture: { type: String, required: true },
  role: { type: String, required: true },
  status: { type: String, required: true },
  createdOn: { type: Date, required: true },
  updatedOn: { type: Date, required: true },
}, { collection: 'users' });
