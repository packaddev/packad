/**
 * exports mongoes schema for user status.
 * @type {Object}
 */

/**
 * For more infomation about how to configure a schema refer
 *   - http://mongoosejs.com/docs/guide.html
 */

var mongoose = require('mongoose');

module.exports = mongoose.Schema({
  name: { type: String, required: true },
  code: { type: Number, required: true },
}, { collection: 'userStatus', strict: false });
