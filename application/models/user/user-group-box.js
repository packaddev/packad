var mongoose = require('mongoose');
var groupBox = require('./group-box.js');

module.exports = mongoose.Schema({
  userId: { type: String, required: true },
  groupBox: [typeof groupBox]
}, { collection: 'userGroupBoxes', strict: true });
