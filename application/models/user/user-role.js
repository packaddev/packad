/**
 * exports mongoes schema for user role.
 * @type {Object}
 */

/**
 * For more infomation about how to configure a schema refer
 *   - http://mongoosejs.com/docs/guide.html
 */

var mongoose = require('mongoose');

module.exports = mongoose.Schema({
  roleDesc: { type: String, required: true },
  roleId: { type: Number, required: true },
}, { collection: 'userRoles', strict: false });
