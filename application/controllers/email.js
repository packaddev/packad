var nodemailer = require('nodemailer');
var config = require('../../config/config.js');

var emailService = {
  sendMail: function(mailOptions, callbackFunc) {
    console.log('\n email.sendMail :: method entry');
    var transporter = nodemailer.createTransport('smtp://' + config.smtpuserid + ':' + config.smtppassword + '@' + config.smtpserver);

    mailOptions.from = config.smtpnoreplyuserid;

    transporter.sendMail(mailOptions, function(error, info) {
      if (error) {
        console.log(error);
      } else {
        callbackFunc(null, info);
        console.log('\n email.sendMail :: method exit: ' + info.response);
      };
    })
  },

  sendTestMail: function(req, res) {
    var transporter = nodemailer.createTransport('smtp://' + config.smtpuserid + ':' + config.smtppassword + '@' + config.smtpserver);

    var mailOptions = {
      from: config.smtpnoreplyuserid,
      to: 'naveen.mandava@tallgrasssoft.com', // list of receivers
      subject: 'PackAd test Email', // Subject line
      text: 'Hello welcome to PackAd \n\n', // plaintext body
    }
    transporter.sendMail(mailOptions, function(error, info) {
      if (error) {
        console.log(error);
        throw error;
      } else {
        console.log('Sent test Email: ' + info.response);
      };
    })
    res.send('Email sent');
  }
};

module.exports = emailService;
