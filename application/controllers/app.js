/**
 * 
 */

'use strict';

/*
 *  Controller which handles api requests coming from the router.
 */
var db = require('../lib/db.js');

module.exports = {
  get: function(req, res) {
    res.render('index');
  }
};
