var db = require('../../lib/db.js');
var appUtils = require('../../lib/app-utils.js');

var userUtil = {
  transformUserObj: function(sourceObj) {
    var uploadDefaults = appUtils.getConfig().upload.user;

    var UserClass = db.getUserClass();
    //var UserStatusClass = db.getUserStatus();
    var Address = db.getAddressClass();

    var userObj = new UserClass();
    userObj.firstName = sourceObj.firstName;
    userObj.lastName = sourceObj.lastName;
    userObj.middleName = sourceObj.middleName;
    userObj.fullName = sourceObj.fullName;
    userObj.genderId = sourceObj.gender;
    userObj.email = sourceObj.email;
    userObj.password = sourceObj.password; //ToDo: Implement hash 'bcrypt'
    userObj.companyName = sourceObj.companyName;
    userObj.businessType = sourceObj.businessType;
    userObj.address = new Address({
      street: sourceObj.street,
      city: sourceObj.city,
      stateId: sourceObj.state.id,
      countryId: sourceObj.country.id,
      zipcode: sourceObj.zipcode
    });
    userObj.website = sourceObj.website;
    userObj.role = sourceObj.role;
    userObj.status = sourceObj.status || 'Pending';
    userObj.createdOn = new Date().getUTCDate();
    userObj.updatedOn = new Date().getUTCDate();

    userObj.profilePicture = sourceObj.profilePicture || uploadDefaults.defaultCompanyLogo;
    if (userObj.role == 'Consumer')
      userObj.profilePicture = userObj.genderId == '1' ? uploadDefaults.defaultMaleProfilePic : uploadDefaults.defaultFemaleProfilePic;

    return userObj;
  }
}

module.exports = userUtil;
