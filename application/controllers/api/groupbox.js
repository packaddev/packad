var db = require('../../lib/db.js');
var errorUtils = require('../../lib/err-util.js');
var _ = require('lodash');

var groupBox = {
  /**
   * connect groupbox details
   */
  connect: function(req, res) {
    console.log('\n Saving groupbox details :: method entry');

    /**
     * reading request details from body
     */
    var UserGroupBoxClass = db.getUserGroupBoxClass();
    var GroupBoxClass = db.getGroupBoxClass();

    var userGroupBoxModel = new UserGroupBoxClass();
    var groupBoxModal = new GroupBoxClass();

    groupBoxModal.businessCategoryId = req.body.businessCategoryId;
    groupBoxModal.businessSubCategoryId = req.body.businessSubCategoryId;
    groupBoxModal.isActive = req.body.isActive;
    groupBoxModal.boxNumber = req.body.boxNumber;

    userGroupBoxModel.userId = req.body.userId;

    /**
     * Checking user group boxes already exists
     */
    UserGroupBoxClass.findOne({ userId: req.body.userId }, function(err, userGroupBox) {
      if (err) {
        errorUtils.handleCustomError(res, 412, 'Error while fetching group box details of a user.');
      }

      if (userGroupBox != null) {

        /** 
         * Update existing user group box object
         */
        console.log('User object found :::');
        console.log(userGroupBox);

        var index = _.findIndex(userGroupBox.groupBox, {
          'businessCategoryId': groupBoxModal.businessCategoryId,
          'businessSubCategoryId': groupBoxModal.businessSubCategoryId
        });

        if (index != -1) {
          return errorUtils.handleCustomError(res, 412, 'Duplicate sub category cannot be saved.');
        }

        console.log('No groupBox exists object found :::');
        userGroupBox.groupBox.push(groupBoxModal);

        userGroupBox.save(function(err) {
          if (err) {
            errorUtils.handleCustomError(res, 412, err);
          } else {
            res.status(200).jsonp({ 'userGroupBoxModel': userGroupBoxModel });
            console.log('\n Group box Details saved successfully.');
          }
        });
      } else {
        /**
         * reating a new user group box object
         * @type {[type]}
         */
        console.log('No user object found :::');
        userGroupBoxModel.groupBox = [];
        userGroupBoxModel.groupBox.push(groupBoxModal);

        userGroupBoxModel.save(function(err) {
          if (err) {
            errorUtils.handleCustomError(res, 412, err);
          } else {
            res.status(200).jsonp({ 'userGroupBoxModel': userGroupBoxModel });
            console.log('\n Group box Details saved successfully.');
          }
        });
      }
    });
  },
  /**
   * disconnect a  groupbox details of a user
   */
  disconnect: function(req, res) {
    console.log('\n Delete groupbox details :: method entry');

    /**
     * reading request details from body
     */
    var UserGroupBoxClass = db.getUserGroupBoxClass();
    var GroupBoxClass = db.getGroupBoxClass();

    var userGroupBoxModel = new UserGroupBoxClass();
    var groupBoxModal = new GroupBoxClass();

    groupBoxModal.businessCategoryId = req.body.businessCategoryId;
    groupBoxModal.businessSubCategoryId = req.body.businessSubCategoryId;

    userGroupBoxModel.userId = req.body.userId;

    /**
     * Checking user group boxes already exists
     */
    UserGroupBoxClass.findOne({ userId: req.body.userId }, function(err, userGroupBox) {
      if (err) {
        return errorUtils.handleCustomError(res, 412, err);
      }

      if (userGroupBox == null) {
        return errorUtils.handleCustomError(res, 412, {
          success: 'false',
          message: 'Unable to find the user groupbox details'
        });
      }

      console.log('User object found :::');

      var index = _.findIndex(userGroupBox.groupBox, {
        'businessCategoryId': groupBoxModal.businessCategoryId,
        'businessSubCategoryId': groupBoxModal.businessSubCategoryId
      });

      if (index != -1) {
        userGroupBox.groupBox.splice(index, 1);

        userGroupBox.save(function(err) {
          if (err) {
            errorUtils.handleCustomError(res, 412, err);
          } else {
            res.status(200).jsonp({ 'userGroupBoxModel': userGroupBoxModel });
            console.log('\n Group box Details saved successfully.');
          }
        });
      }
    });
  },
  /**
   * Getting all groupbox details of user
   */
  getAll: function(req, res) {
    console.log('\n Get all groupbox details of user:: method entry');

    var UserGroupBoxClass = db.getUserGroupBoxClass();
    var userGroupBoxModel = new UserGroupBoxClass();

    UserGroupBoxClass.findOne({ userId: req.params.id }, function(err, userGroupBox) {
        if (err) {
          errorUtils.handleCustomError(res, 412, 'Unable to find group box details of a user.');
        }
        if (!userGroupBox) {
          return res.status(200).jsonp({});
        } else {
          var clientResp = JSON.parse(JSON.stringify(userGroupBox));
          console.log('\nGet all groupbox details of user :: method exit');
          return res.status(200).jsonp(clientResp);
        }
      }) //FindOne end
  }
}

module.exports = groupBox;
