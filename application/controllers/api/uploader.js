var fs = require('fs');
var db = require('../../lib/db.js');
var errorUtils = require('../../lib/err-util.js');
var mime = require('mime');


/**
 * uploadUtility - utility method to upload a file
 * @param  {Object}   file     File to be read
 * @param  {String}   dir      Place where a file has to be written
 * @param  {Function} callBack Call back after success/failure
 */
var uploadUtility = function(file, dir, callBack) {
  /**
   * is - Input stream for the file to be read
   * @type {Object}
   */
  var is = fs.createReadStream(file.path);

  /**
   * fileName - File name with out extention
   * @type {String}
   */
  var fileName = file.name.split('.')[0];
  /**
   * extension - Extension of the file that is being read/written
   * @type {String}
   */
  var extension = '.' + file.name.split('.')[1];
  /**
   * renaming the file name that has to be written to desired folder
   * adding time in the name to makefiles unique.
   * we can further encrypt file name in future if needed
   */
  fileName = (fileName + ' ' + new Date().getTime() + extension).replace(/\s+/g, '_');
  /**
   * os - Output stream to write the file
   * @type {Object}
   */
  var os = fs.createWriteStream(dir + fileName);

  /**
   * even handler when input stream is open
   */
  is.on('open', function() {
    console.log('Opened the file \'' + file.name + '\' for reading.');
  });

  /**
   * handler when error occurs while reading the input from temp location
   * @param  {Object} err - error Object describing the error occcured while reading file(stream)
   */
  is.on('error', function(err) {
    if (err) {
      callBack(err);
    }
  });

  /**
   * Handler to know when reading a file stream is complete
   */
  is.on('end', function() {
    /**
     * Unlinking the read stream as soon as the file/stream is completely read
     */
    fs.unlinkSync(file.path, function() {
      console.log('Unlinked input stream from \'' + file.name + '\'.');
    });
  });

  /**
   * even handler when output stream is open
   */
  os.on('open', function() {
    console.log('Opened the file \'' + file.name + '\'  at \'' + dir + '\'for writing.');
  });

  /**
   * handler when error occurs while writing into desired location
   * @param  {Object} err - error Object describing the error occcured while writing file(stream)
   */
  os.on('error', function(err) {
    if (err) {
      callBack(err);
    }
  });

  /**
   * handler when output stream is written
   * Ideally file is written
   */
  os.on('finish', function() {
    console.log('Written file in the location \'' + dir + '\'.');
    callBack(null, os);
  });


  /**
   * Input stream is sent to output stream.
   * which fires the above events.
   */
  is.pipe(os);
};


var uploader = {
  /**
   * user - end point to upload profile pic of user
   */
  user: function(req, res) {
    /**
     * appDefaults - holds application degaults
     * @type {Object}
     */
    var appDefaults = require('../../lib/app-utils.js').getConfig();
    /**
     * userUploadDefaults - Holds defaults specific to uploading user
     * @type {Object}
     */
    var userUploadDefaults = appDefaults.upload.user;
    /**
     * userId - holds ID of user to be updated
     * @type {String}
     */
    var userId = req.params.id;

    /**
     * Call the utility method that actually writes the desired output
     */
    uploadUtility(req.files.file, userUploadDefaults.dest, function(error, response) {
      if (error) {
        return errorUtils.handleCustomError(res, 412, error);
      }

      /**
       * UserClass - get user class from DB
       * @type {Object}
       */
      var UserClass = db.getUserClass();

      /**
       * Find the user with provide email address
       */
      UserClass.findById(userId, function(err, user) {
        if (err) {
          return errorUtils.handleError(res, err);
        }
        /**
         * if user is not found in the data base
         */
        if (!user) {
          return errorUtils.handleCustomError(res, 412, {
            success: 'false',
            message: 'User with email - ' + email + ' is not found'
          });
        }

        /**
         * update user with latest profile pic location
         */
        user.profilePicture = userUploadDefaults.urlRoute + response.path.split('/').pop();

        user.save(function(err) {
          if (err) {
            return errorUtils.handleCustomError(res, 412, err);
          }
          /**
           * Setting response headders according to the files uploaded
           */
          res.setHeader('Content-Disposition', 'inline; filename =' + req.files.file.name);
          res.setHeader('Content-Type', mime.lookup(req.files.file.name));

          return res.status(200).jsonp({
            status: 'sucess',
            message: 'Upload successful'
          });
        });
      });
    });
  }
}

module.exports = uploader;
