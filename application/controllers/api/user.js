var db = require('../../lib/db.js');
var emailService = require('../email.js');
var mailConstants = require('../../templates/email-constants.js');
var utilities = require('../../lib/util.js');
var errorUtils = require('../../lib/err-util.js');
var userUtil = require('../util/user-util.js');



var user = {
  /**
   * logIn - end point used to log in a user
   */
  logIn: function(req, res) {
    console.log('\n api.user.logIn :: method entry');

    var email = req.body.email;
    var pass = req.body.password;

    var UserClass = db.getUserClass();

    /**
     * Find the user with provide email address
     */
    UserClass.findOne({
      'email': email
    }, function(err, user) {
      if (err) {
        return errorUtils.handleError(res, err);
      }
      /**
       * if user is not found in the data base
       */
      if (!user) {
        return errorUtils.handleCustomError(res, 412, {
          success: 'false',
          message: 'User with email - ' + email + ' is not found'
        });
      }

      /**
       * Check if the password is entered properly
       */
      if (user.password != pass) {
        return errorUtils.handleCustomError(res, 412, {
          success: 'false',
          message: 'Password entered is not correct.'
        });
      }

      /**
       * setting session variable
       */
      req.session['loggedInUser'] = user;
      req.session['sessionKey'] = utilities.createSessionKey(user.email, user.companyName, user.password);

      /**
       * In the schema defined for user we have password
       * hence we cannot directly delete password from user and sent to front end
       *
       * Taking a copy of the user object and removing all the
       * fields that should not be exposed to client.
       * Below is the list :
       *
       *  - Password
       */
      var clientResp = JSON.parse(JSON.stringify(user));
      delete clientResp.password;
      console.log('\n api.user.getOne :: method exit');
      return res.status(200).jsonp({
        user: clientResp,
        key: req.session['sessionKey']
      });
    });
  },
  /**
   * logOut - End point for logging out a user
   */
  logOut: function(req, res) {
    console.log('\n api.user.logOut :: method entry');

    req.session['loggedInUser'] = undefined;
    req.session['sessionKey'] = undefined;

    console.log('\n api.user.logOut :: method exit');
    return res.status(200).jsonp({
      'status': 'User logged out successfully'
    });
  },
  /**
   * create - endpoint used to create user in DB
   */
  create: function(req, res) {
    console.log('\n api.user.create :: method entry');
    /**
     * Transform the user object according to the model defined from request
     */
    var userObj = userUtil.transformUserObj(req.body);

    userObj.save(function(err) {
      if (err) {
        return errorUtils.handleCustomError(res, 412, err);
      }

      /**
       * To be changed
       */
      req.params.userId = userObj.email;

      /**
       * setting session variable
       */
      req.session['loggedInUser'] = userObj;
      req.session['sessionKey'] = utilities.createSessionKey(userObj.email, userObj.createdOn.toString(), userObj.password);

      sendEmailConfirmation(req, res);

      /**
       * In the schema defined for user we have password
       * hence we cannot directly delete password from user and sent to front end
       *
       * Taking a copy of the user object and removing all the
       * fields that should not be exposed to client.
       * Below is the list :
       *
       *  - Password
       */
      var clientResp = JSON.parse(JSON.stringify(userObj));
      delete clientResp.password;
      console.log('\n api.user.create :: method exit');
      return res.status(200).jsonp({ 'user': clientResp, key: req.session['sessionKey'] });
    });
  },
  /**
   * getOne - end point for retrieving user from database
   */
  getOne: function(req, res) {
    console.log('\napi.user.getOne :: method entry');

    var UserClass = db.getUserClass();

    /**
     * Find the user with provide email address
     */
    UserClass.findById(req.params.id, function(err, user) {
      if (err) {
        return errorUtils.handleError(res, err);
      }
      /**
       * if user is not found in the data base
       */
      if (!user) {
        return errorUtils.handleCustomError(res, 412, {
          success: 'false',
          message: 'User with email - ' + email + ' is not found'
        });
      }

      /**
       * In the schema defined for user we have password
       * hence we cannot directly delete password from user and sent to front end
       *
       * Taking a copy of the user object and removing all the
       * fields that should not be exposed to client.
       * Below is the list :
       *
       *  - Password
       */
      var clientResp = JSON.parse(JSON.stringify(user));
      delete clientResp.password;
      console.log('\napi.user.getOne :: method exit');
      return res.status(200).jsonp(clientResp);
    });
  },
  update: function(req, res) {
    return res.status(200).jsonp(req.body);
  },
  getAll: function(req, res) {
    var UserClass = db.getUserClass();
    UserClass.find({}, function(err, data) {
      if (err)
        throw err;

      res.send(data);
    });

  },
  emailConfirmation: function(req, res) {
    var decryptedVerStr = utilities.decrypt(req.params.verstr);
    console.log('Email verification received for (decrypt): ' + decryptedVerStr);

    var UserClass = db.getUserClass();
    UserClass.findOne({ email: decryptedVerStr }, function(err, user) {
        if (err || !user) {
          errorUtils.handleCustomError(res, 412, 'Email verification failed !!, Please try again or contact PackAd customer care');
        } else {
          user.status = 'Confirmed';
          user.save(function(err) {
              if (err)
                res.send('<H2>Email verification failed !!, Please try again or contact PackAd customer care</H2>');
              else
                res.send('<H2>Email verification completed, Please log in to PackAd </H2>');
            }) //Save end
        }
      }) //FindOne end

    //res.send('Email verification completed, Please log in to PackAd ');
  },
  /**
   * sendEmailVerification - End point to send verification mail again
   */
  sendEmailVerification: function(req, res) {
    console.log('\n api.user.sendEmailVerification :: method entry');
    var userId = req.body.email;
    console.log('Email verification sending for : ' + userId);

    var UserClass = db.getUserClass();
    var userObj;
    UserClass.findById(req.body._id, function(err, user) {
      if (err || !user) {
        console.log('Not able to get user from DB :' + userId);
        errorUtils.handleCustomError(res, 412, {
          success: false,
          message: 'Not able to get user from DB'
        });
      } else {
        userObj = user;

        var encryptedVerStr = utilities.encrypt(userObj.email);
        var mailConsts = new mailConstants();
        var mailOptions = {
          from: '',
          to: userObj.email,
          subject: mailConsts.confirmationSubject,
          html: mailConsts.replaceKeys(mailConsts.confirmationBody, { Name: userObj.companyName, VerStr: encryptedVerStr })
        }
        emailService.sendMail(mailOptions, function(err) {
          if (err) {
            errorUtils.handleError(res, err);
          }
          console.log('\n api.user.sendEmailVerification :: method exit');
          res.status(200).jsonp(user)
        });

      }
    });
  },
  /**
   * checkEmailExist - end point method to send verification email after log In
   */
  checkEmailExist: function(req, res) {
    console.log('\n api.user.checkEmailExist :: method entry');
    var UserClass = db.getUserClass();
    /**
     * Find user based on email ID.
     * return  response based on user existance
     */
    UserClass.findOne({ email: req.body.email }, function(err, user) {
      if (err) {
        return errorUtils.handleCustomError(res, 412, 'Email check failed');
      }
      console.log('\n api.user.checkEmailExist :: method exit');
      if (user == null) {
        /**
         * User not fount
         */
        return res.status(200).jsonp({
          'emailRegistered': false
        });
      } else {
        /**
         * User found
         */
        return res.status(200).jsonp({
          'emailRegistered': true
        });
      }
    });
  }
}

function sendEmailConfirmation(req, res) {

  var userId = req.params.userId;
  console.log('Email verification sending for : ' + userId);

  var UserClass = db.getUserClass();
  var userObj;
  UserClass.findOne({ email: userId }, function(err, user) {
    if (err || !user) {
      console.log('Not able to get user from DB :' + userId);
      errorUtils.handleCustomError(res, 412, {
        success: false,
        message: 'Not able to get user from DB'
      });
    } else {
      userObj = user;

      var encryptedVerStr = utilities.encrypt(userObj.email);
      var mailConsts = new mailConstants();
      var mailOptions = {
        from: '',
        to: userObj.email,
        subject: mailConsts.confirmationSubject,
        html: mailConsts.replaceKeys(mailConsts.confirmationBody, { Name: userObj.companyName, VerStr: encryptedVerStr })
      }
      emailService.sendMail(mailOptions, function(err) {});

    }
  });
}

module.exports = user;
