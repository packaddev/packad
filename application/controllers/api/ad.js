var db = require('../../lib/db.js');
var emailService = require('../email.js');
var mailConstants = require('../../templates/email-constants.js');
var utilities = require('../../lib/util.js');
var errorUtils = require('../../lib/err-util.js');
var adUtil = require('../../lib/ad-util.js');



var ad = {
  /**
   * create - endpoint used to create Ad in DB
   */
  create: function(req, res) {
    console.log('\n api.ad.create :: method entry');
    /**
     * Transform the Ad object according to the model defined from request
     */
    /**
     * Ad - Ad Class
     * @type {Object}
     */
    var Ad = db.getAdClass();
    /**
     * ad - Instance of Ad class
     * @type {Object}
     */
    var ad = new Ad();

    /**
     * Form ad according to admodel
     */
    ad = adUtil.formAd(ad, req);

    ad.save(function(err) {
      if (err) {
        return errorUtils.handleCustomError(res, 412, err);
      }
      console.log('\n api.user.create :: method exit');
      return res.status(200).jsonp(ad);
    });
  },
  /**
   * getOne - end point for retrieving user from database
   */
  getOne: function(req, res) {
    console.log('\napi.user.getOne :: method entry');

    var UserClass = db.getUserClass();

    /**
     * Find the user with provide email address
     */
    UserClass.findById(req.params.id, function(err, user) {
      if (err) {
        return errorUtils.handleError(res, err);
      }
      /**
       * if user is not found in the data base
       */
      if (!user) {
        return errorUtils.handleCustomError(res, 412, {
          success: 'false',
          message: 'User with email - ' + email + ' is not found'
        });
      }

      /**
       * In the schema defined for user we have password
       * hence we cannot directly delete password from user and sent to front end
       *
       * Taking a copy of the user object and removing all the
       * fields that should not be exposed to client.
       * Below is the list :
       *
       *  - Password
       */
      var clientResp = JSON.parse(JSON.stringify(user));
      delete clientResp.password;
      console.log('\napi.user.getOne :: method exit');
      return res.status(200).jsonp(clientResp);
    });
  },
  update: function(req, res) {
    return res.status(200).jsonp(req.body);
  },
  getAll: function(req, res) {
    var UserClass = db.getUserClass();
    UserClass.find({}, function(err, data) {
      if (err)
        throw err;

      res.send(data);
    });

  }
}

function sendEmailConfirmation(req, res) {

  var userId = req.params.userId;
  console.log('Email verification sending for : ' + userId);

  var UserClass = db.getUserClass();
  var userObj;
  UserClass.findOne({ email: userId }, function(err, user) {
    if (err || !user) {
      console.log('Not able to get user from DB :' + userId);
      errorUtils.handleCustomError(res, 412, {
        success: false,
        message: 'Not able to get user from DB'
      });
    } else {
      userObj = user;

      var encryptedVerStr = utilities.encrypt(userObj.email);
      var mailConsts = new mailConstants();
      var mailOptions = {
        from: '',
        to: userObj.email,
        subject: mailConsts.confirmationSubject,
        html: mailConsts.replaceKeys(mailConsts.confirmationBody, { Name: userObj.companyName, VerStr: encryptedVerStr })
      }
      emailService.sendMail(mailOptions, function(err) {});

    }
  });
}

module.exports = ad;
