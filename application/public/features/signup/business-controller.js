angular.module('signUp')
  .controller('BusinessController', ['$scope', 'user', 'filterFilter', 'commonDataService',
    function(scope, resolvedUser, filterFilter, commonDataService) {
      var ct = this;
      ct.user = resolvedUser;
      ct.user.role = 'Business';
      ct.selectedTabId = 1;

      commonDataService.getCountries()
        .success(function(response) {
          ct.countries = response;
        })
        .error(function(error) {
          console.log('Unable to load country data: ' + error.message);
        });

      commonDataService.getStates()
        .success(function(response) {
          ct.states = response;
        })
        .error(function(error) {
          console.log('Unable to load states data: ' + error.message);
        });

      commonDataService.getBusinessCategories()
        .success(function(response) {
          ct.businessCategories = response;
          ct.user.businesstype = ct.businessCategories[0].category;
        })
        .error(function(error) {
          console.log('Unable to load business categories data: ' + error.message);
        });

      ct.saveUser = function() {
        ct.image = ct.image || undefined;
        ct.user.makeSave(ct.image);
      };

      ct.nextClick = function() {
        ct.selectedTabId = 2;
      };

      ct.checkEmailVerification = function() {
        ct.user.checkExistingEmail(function(response) {
          if (response.emailRegistered) {
            ct.emailRegistered = true;
          }
        });
      }
      ct.previousClick = function() {
        var isFormValidated = true;
        if (isFormValidated) {
          ct.selectedTabId = 1;
        }
      };

      /**
       * uploadProfile - triggered when profile pic is changed
       * @param  {[File]} displayPic - File that is bring uploaded
       */
      ct.uploadProfile = function(displayPic) {
        /**
         * Leaving empty as
         *
         * can be used when a use changes his DP.
         */
      };
    }
  ]);
