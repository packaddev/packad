angular.module('signUp', ['ui.router', 'ngFileUpload'])
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

    /**
     * signup.business - states gets loaded when we have a business user 
     *                   sign up process
     */
      .state('signup.business', {
        url: '/Business',
        views: {
          'header': {
            templateUrl: './features/signup/partials/header.html'
          },
          'body': {
            templateUrl: './features/signup/partials/business.html',
            controller: 'BusinessController',
            controllerAs: 'busCt'
          }
        }
      })
      /**
       * signup.consumer - states gets loaded when we have a consumer 
       *                   sign up process
       */
      .state('signup.consumer', {
        url: '/Consumer',
        views: {
          'header': {
            templateUrl: './features/signup/partials/header.html'
          },
          'body': {
            templateUrl: './features/signup/partials/consumer.html',
            controller: 'ConsumerController',
            controllerAs: 'ctrl'
          }
        }
      })
  });
