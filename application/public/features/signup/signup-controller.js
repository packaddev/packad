/**
 *
 *   Sign up controller
 *
 *   This is the controller used for the sign up page.
 *   For all the users
 *   It has functionalities for creating a user, etc,.
 *
 */

angular.module('signUp')
  .controller('SignUpController', ['$scope', '$state', '$location', '$window', 'user', function(scope, state, location, window, resolvedUser) {
    /**
     * Code to be written common to consumer/business user sign up
     */
  }]);
