angular.module('packAdApp')
  .directive('consumerGroupbox', function() {

    var directive = {};

    //GroupBox Element
    directive.restrict = 'E';

    directive.templateUrl = '../features/consumerHome/directives/groupbox.html';

    directive.scope = {
      groupboxDetails: '=',
      user: '='
    }

    directive.controller = function($scope, GroupBoxService) {
      $scope.Disconnect = function() {
        GroupBoxService.disconnect({
          businessCategoryId: $scope.groupboxDetails.businessCategoryId,
          businessSubCategoryId: $scope.groupboxDetails.businessSubCategoryId,
          userId: $scope.user._id
        });

        $scope.groupboxDetails.groupBoxCategory = "";
        $scope.groupboxDetails.groupBoxSubCategory = "";
        $scope.groupboxDetails.groupBoxText = "Connect";
        $scope.groupboxDetails.categoryImageUrl = "";
        $scope.groupboxDetails.businessCategoryId = -1;
        $scope.groupboxDetails.businessSubCategoryId = -1;
      };

      $scope.createAd = function() {
        if ($scope.groupboxDetails.groupBoxText == 'Disconnect') {
          $scope.user.modal.open({
            url: 'features/ad/modal-holder.html',
            resolve: {
              details: $scope.groupboxDetails,
              purpose: 'adCreation'
            }
          });
        } else {
          $('#connectFirst').modal('show');
        }
      }
    }

    //compile is called during application initialization. AngularJS calls it once when html page is loaded.

    directive.compile = function(element, attributes) {


      //linkFunction is linked with each element with scope to get the element specific data.
      var linkFunction = function(gbScope, element, attributes, controller) {
        // console.log(gbScope.groupboxDetails);
        // console.log(gbScope.user);
      }
      return linkFunction;
    }

    return directive;
  });
