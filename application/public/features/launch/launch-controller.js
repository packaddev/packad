/**
 * 
 *   Launch controller
 *
 *   This is the controller used for the default page.
 *   It has functionalities for logging a user 
 *   redirection of a user to sign up page, etc,.
 * 
 */


angular.module('packAdApp')
  .controller('LaunchController', ['paUser', '$window', '$state', 'AlertService', function(paUser, window, state, AlertService) {

    var lc = this;

    lc.isLoginSuccess = true;

    /**
     * credentials - instancce of Pack ad user
     *               This is used only to get the inputs provided by user in launch page
     * @type {Object}
     */
    lc.credentials = new paUser({
      email: '',
      password: ''
    });

    /**
     * login - Used for loggin in the user into application
     */
    lc.login = function() {
      lc.credentials.$logIn()
        .then(function(response) {
          lc.isLoginSuccess = true;
          window.sessionStorage.setItem('accessKey', response.key);
          window.sessionStorage.setItem('id', response.user._id)
          if(response.user.role == 'Consumer')
          {
              state.go('consumerHome', {
                'id': response.user._id
              });
          }
          else
          {
              state.go('home', {
                'id': response.user._id
              });
          }
        }, function(error) {
          lc.isLoginSuccess = false;
          AlertService.addWarning('Login failed...', null, null);
        });
    };

    lc.do = function($event) {
      $event.preventDefault();
    };

  }]);
