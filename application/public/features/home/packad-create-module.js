angular.module('packAdApp').controller('ModalDemoCtrl', function ($scope, $uibModal, $log) {

  $scope.items = 
  [
  'item1', 
  'item2', 
  'item3', 
  'item4', 
  'item4', 
  'item4', 
  'item5', 
  'item6', 
  'item7', 
  'item8', 
  'item9', 
  'item10',
  'item11',
  'item12',
  'item13',
  'item14',
  'item15',
  'item16',
  'item17',
  'item18',
  'item19',
  'item20',
  'item21'
  ];

  $scope.opportunity = '';
  $scope.isShown = function(opportunity) {
  return opportunity === $scope.opportunity;
  };


  $scope.animationsEnabled = true;

  $scope.open = function (size) {

    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'myModalContent.html',
      controller: 'ModalInstanceCtrl',
      size: size,
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };

  $scope.toggleAnimation = function () {
    $scope.animationsEnabled = !$scope.animationsEnabled;
  };


});

// Please note that $uibModalInstance represents a modal window (instance) dependency.
// It is not the same as the $uibModal service used above.

angular.module('packAdApp').controller('ModalInstanceCtrl', function ($scope, $uibModalInstance, items) {

  $scope.items = items;
  $scope.selected = {
    item: $scope.items[0]
  };

  $scope.ok = function () {
    $uibModalInstance.close($scope.selected.item);
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});




angular.module('packAdApp').controller("WizardController", [wizardController]);

    function wizardController() {
        var vm = this;
        
        //Model
        vm.currentStep = 1;
        vm.steps = [
          {
            step: 1,
            name: "First step",
            template: "../features/home/partials/step1.html"
          },
          {
            step: 2,
            name: "Second step",
            template: "../features/home/partials/step2.html"
          },   
          {
            step: 3,
            name: "Third step",
            template: "../features/home/partials/step3.html"
          },
           {
            step: 4,
            name: "Fourth step",
            template: "../features/home/partials/step4.html"
          }, 
           {
            step: 5,
            name: "5",
            template: "../features/home/partials/step5.html"
          },{
            step: 6,
            name: "6",
            template: "../features/home/partials/step6.html"
          },{
            step: 7,
            name: "7",
            template: "../features/home/partials/step7.html"
          },{
            step: 8,
            name: "8",
            template: "../features/home/partials/step8.html"
          },{
            step: 9,
            name: "9",
            template: "../features/home/partials/step9.html"
          },{
            step: 10,
            name: "10",
            template: "../features/home/partials/step10.html"
          },{
            step: 11,
            name: "11",
            template: "../features/home/partials/step11.html"
          },{
            step: 12,
            name: "12",
            template: "../features/home/partials/step12.html"
          },{
            step: 13,
            name: "13",
            template: "../features/home/partials/step13.html"
          },{
            step: 14,
            name: "14",
            template: "../features/home/partials/step14.html"
          },{
            step: 15,
            name: "15",
            template: "../features/home/partials/step15.html"
          },{
            step: 16,
            name: "16",
            template: "../features/home/partials/step16.html"
          },{
            step: 17,
            name: "17",
            template: "../features/home/partials/step17.html"
          },{
            step: 18,
            name: "18",
            template: "../features/home/partials/step18.html"
          },{
            step: 19,
            name: "19",
            template: "../features/home/partials/step19.html"
          },{
            step: 20,
            name: "20",
            template: "../features/home/partials/step20.html"
          },{
            step: 21,
            name: "21",
            template: "../features/home/partials/step21.html"
          } 
                        
        ];
        vm.user = {};
        
        //Functions
        vm.gotoStep = function(newStep) {
          vm.currentStep = newStep;
        }
        
        vm.getStepTemplate = function(){
          for (var i = 0; i < vm.steps.length; i++) {
                if (vm.currentStep == vm.steps[i].step) {
                    return vm.steps[i].template;
                }
            }
        }
        
        vm.save = function() {
          alert(
            "Saving form... \n\n" + 
            "Name: " + vm.user.name + "\n" + 
            "Email: " + vm.user.email + "\n" + 
            "Age: " + vm.user.age);
        }

        
        vm.getNextLabel = function() {
            $('.selectpicker').selectpicker();
            
            if (vm.currentStep === 1) {
              angular.element(document).find('#next').text('GO');
              angular.element(document).find('#prev').hide();
              angular.element(document).find('#cancelChange').hide();
              
              
            } else if(vm.currentStep === 2){
              angular.element(document).find('#next').text('Confirm & Continue');
              angular.element(document).find('#prev').hide();
              angular.element(document).find('#cancelChange').hide();
            
            } else if(vm.currentStep === 3){
              angular.element(document).find('#next').text('Change & Continue');
              angular.element(document).find('#cancelChange').show();
              angular.element(document).find('#prev').hide();
            
            } else if(vm.currentStep === 6){
              angular.element(document).find('#next').text('Next');
              angular.element(document).find('#cancelChange').hide();
              angular.element(document).find('#prev').show().text('Back');
            
            } else if(vm.currentStep === 9){
              angular.element(document).find('#next').show();
              angular.element(document).find('#cancelChange').hide();
              angular.element(document).find('#prev').show().text('Back');
            
            }else if(vm.currentStep === 10){
              angular.element(document).find('#next').show();
              angular.element(document).find('#cancelChange').hide();
              angular.element(document).find('#prev').show().text('Back');
            }

          }


    }


 /**
 * added table row frm http://lorenzofox3.github.io/'/'
 */
angular.module('packAdApp').controller('safeCtrl', ['$scope', function ($scope) {

    var sizes = ['Laurent', 'Blandine', 'Olivier', 'Max'];
    var colors = ['Renard', 'Faivre', 'Frere', 'Eponge'];
    var qtys = ['1987-05-21', '1987-04-25', '1955-08-27', '1966-06-06'];
    var id = 1;

    function generateRandomItem(id) {

        var size = sizes[Math.floor(Math.random() * 3)];
        var color = colors[Math.floor(Math.random() * 3)];
        var qty = qtys[Math.floor(Math.random() * 3)];

        return {
            id: id,
            sizeH: size,
            colorH: color,
            qtyH: qty
        }
    }

    $scope.rowCollection = [];

    for (id; id < 1; id++) {
        $scope.rowCollection.push(generateRandomItem(id));
    }

    //add to the real data holder
    $scope.addRandomItem = function addRandomItem() {
        $scope.rowCollection.push(generateRandomItem(id));
        id++;
    };

    //remove to the real data holder
    $scope.removeItem = function removeItem(row) {
        var index = $scope.rowCollection.indexOf(row);
        if (index !== -1) {
            $scope.rowCollection.splice(index, 1);
        }
    }
}]);



angular.module('packAdApp').controller('TooltipDemoCtrl', function ($scope, $sce) {
  $scope.dynamicTooltip = 'Hello, World!';
  $scope.dynamicTooltipText = 'dynamic';
  $scope.htmlTooltip = $sce.trustAsHtml('I\'ve been made <b>bold</b>!');
  $scope.placement = {
    options: [
      'top',
      'top-left',
      'top-right',
      'bottom',
      'bottom-left',
      'bottom-right',
      'left',
      'left-top',
      'left-bottom',
      'right',
      'right-top',
      'right-bottom'
    ],
    selected: 'top'
  };
});

