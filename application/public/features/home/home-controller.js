angular.module('packAdApp')
  .controller('HomeController', function($scope, $window, $state, $filter, user, businessCategories, businessSubCategories, GroupBoxService, groupBoxDetailsList) {
    var self = this;

    self.groupBoxDetailsArray = [];

    if (groupBoxDetailsList.data.groupBox) {
      //var filteredGroupboxDetailsList = $filter('orderBy')(groupBoxDetailsList.data.groupBox, 'boxNumber');

      angular.forEach(groupBoxDetailsList.data.groupBox, function(value, key) {
        if (value) {
          var categoryName = $filter('filter')(businessCategories.data, { Id: value.businessCategoryId.toString() }, true)[0].Name;
          var subCategoryName = $filter('filter')(businessSubCategories.data, { Id: value.businessSubCategoryId.toString() }, true)[0].Name;

          self.groupBoxDetailsArray.push({
            boxNumber: self.groupBoxDetailsArray.length,
            isActive: value.isActive,
            groupBoxCategory: categoryName,
            groupBoxSubCategory: subCategoryName,
            groupBoxText: "Disconnect",
            categoryImageUrl: "../images/defualt-icon.png",
            businessCategoryId: value.businessCategoryId,
            businessSubCategoryId: value.businessSubCategoryId
          });
        };
      });
    }

    if (self.groupBoxDetailsArray.length < 4) {
      var count = 4 - self.groupBoxDetailsArray.length;

      for (i = 0; i < count; i++) {
        self.groupBoxDetailsArray.push({
          boxNumber: self.groupBoxDetailsArray.length,
          isActive: false,
          groupBoxCategory: "",
          groupBoxSubCategory: "",
          groupBoxText: "Connect",
          categoryImageUrl: "",
          businessCategoryId: -1,
          businessSubCategoryId: -1
        });
      }
    }

    self.groupBoxDetailsArray[0].isActive = true;

    $('#businessCategoryModal').on('show.bs.modal', function(e) {
      var boxNumber = $(e.relatedTarget).data('box-number');

      if (boxNumber != undefined || boxNumber != null) {
        self.selectedBoxNumber = boxNumber;
      }
    });

    self.currentUser = user;
    self.currentUser.modal = {
      'message': 'Initial Alert',
      'actionButtons': []
    };

    self.userEmailConfirmed = (self.currentUser.status === 'Confirmed') ? true : false;

    self.emailSent = false;

    self.sendEmailVerification = function() {
      self.currentUser.$getVarified()
        .then(function(response) {
          self.emailSent = true;
        }, function(error) {
          console.log(error);
        });
    }

    self.saveButtonClick = function() {

    }

    self.logout = function() {
      self.currentUser.$logOut()
        .then(function(response) {
          $window.sessionStorage.removeItem('accessKey');
          $window.sessionStorage.removeItem('id');
          $state.go('launch');
        });
    }

    self.subCategoryClick = function(categoryId, subCategoryName) {
      self.selectedSubCategoryName = subCategoryName;
      self.selectedCategoryName = $filter('filter')(businessCategories.data, { Id: categoryId }, true)[0].Name;
      $('#businessCategoryModal').modal('hide');
    }

    self.showCategoryModal = function() {
      $('#conformation').modal('hide');
      $('#businessCategoryModal').modal('show');
    }

    self.tcConfirmed = function() {
      self.groupBoxDetailsArray[self.selectedBoxNumber].groupBoxCategory = self.selectedCategoryName;
      self.groupBoxDetailsArray[self.selectedBoxNumber].groupBoxSubCategory = self.selectedSubCategoryName;
      self.groupBoxDetailsArray[self.selectedBoxNumber].categoryImageUrl = "../images/defualt-icon.png";
      self.groupBoxDetailsArray[self.selectedBoxNumber].groupBoxText = "Disconnect";
      self.groupBoxDetailsArray[self.selectedBoxNumber].businessCategoryId = $filter('filter')(businessCategories.data, { Name: self.selectedCategoryName }, true)[0].Id;
      self.groupBoxDetailsArray[self.selectedBoxNumber].businessSubCategoryId = $filter('filter')(businessSubCategories.data, { Name: self.selectedSubCategoryName }, true)[0].Id;

      $('#businessCategoryModal').modal('hide');

      GroupBoxService.connect({
        businessCategoryId: self.groupBoxDetailsArray[self.selectedBoxNumber].businessCategoryId,
        businessSubCategoryId: self.groupBoxDetailsArray[self.selectedBoxNumber].businessSubCategoryId,
        isActive: true,
        boxNumber: 1,
        userId: self.currentUser._id
      });
    }

    function chunk(arr, size) {
      var newArr = [];
      for (var i = 0; i < arr.length; i += size) {
        newArr.push(arr.slice(i, i + size));
      }
      return newArr;
    }

    self.chunkedBusinessCategoryData = chunk(businessCategories.data || [], 3);
    self.businessSubCategories = businessSubCategories.data || [];


    // self.createAd = function() {
    //   self.currentUser.modal.open({
    //     url: 'features/ad/modal-holder.html',
    //     resolve: {
    //       purpose: 'adCreation'
    //     }
    //   });
    // };
  })
  .filter('businessSubCategoryList', function() {
    return function(businessSubCategories, businessCategoryId) {
      var subCategories = [];
      if (businessSubCategories) {
        for (var i = 0; i < businessSubCategories.length; i++) {
          if (businessSubCategories[i].catId === businessCategoryId) {
            subCategories.push(businessSubCategories[i]);
          }
        }
      }

      return subCategories;
    }

  }); //controller end



