/**
 * 
 *   Pack ad AD service
 *
 *   This file exports a class that can be used for all the operations
 *   on Ad
 *   
 */

angular.module('packAdApp')
  .factory('ad', ['$resource', 'APISERVER', 'Upload', function(resource, apiRoute, uploader) {

    /**
     * url - default route for all the user specific operations
     */
    var url = apiRoute + 'ad/:id';
    var uploaderUrl = apiRoute + 'upload/add/';
    // authorization to be handled
    // var authHeaders = { 'authKey': window.sessionStorage.getItem('accessKey') } || {};

    /**
     * transformRequest - Method for transforming request object 
     *                    before sending to http 
     * @param  {Object} data - data sent to resource method
     * @return {Object}      data sent to http method
     */
    var transformRequest = function(request) {
      // operations on data being sent to http
      return angular.toJson(request);
    };

    /**
     * transformResponse - Method for transdorming the response 
     *                     recieved from http
     * @param  {Object} result - data recieved from http
     * @return {Object}        -  data being sent in the promise
     */
    var transformResponse = function(data) {
      // operations on response from http
      return angular.fromJson(data);
    };

    /**
     * ad - Ad Object 
     *      This object is the class that we expose as a dependency
     * @type {Object}
     */
    var ad = resource(url, { 'id': '@_id' }, {
      save: {
        method: 'POST',
        isArray: false,
        transformRequest: transformRequest,
        transformResponse: transformResponse
      },
      update: {
        method: 'POST',
        isArray: false,
        transformRequest: transformRequest,
        transformResponse: transformResponse
      }
    });

    /**
     * makeSave - Adding methods to the class that will be accessable
     *            when we create a new ad or update existing
     */
    ad.prototype.makeSave = function(image) {
      console.log(this);
      return;
      var operation;
      /**
       * adClient - maintaining current instance
       *            as it will be used after first DB call
       */
      var adClient = this;
      if (this._id) {
        operation = this.$update();
      } else {
        operation = this.$save();
      }

      /**
       * operation is a promise.
       *
       * .then contains two functions
       * 1st is success handler 
       * 2nd is error handler
       */
      operation.then(function(data) {
        console.log(data);
      }, function(err) {
        // We could show model window here
        console.log(err);
      });
    };

    return ad;
  }]);


angular.module('packAdApp')

.controller('AppCtrl', function($scope) {
  $scope.title1 = 'Button';
  $scope.title4 = 'Warn';
  $scope.isDisabled = true;

  $scope.googleUrl = 'http://google.com';

});
