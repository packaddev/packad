angular.module('packAdApp')
  .service('AlertService', function() {
    this.alerts = [];
    this.addWarning = function(message, closeable, timeOut) {
      this.alerts = [{ message: message }];
    };
    this.getAlerts = function() {
      return this.alerts;
    };
  })
