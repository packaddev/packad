angular.module('packAdApp')
  .controller('AlertController', function(AlertService) {
    var self = this;
    // console.log(AlertService);
    self.alerts = AlertService.getAlerts();
  }); //controller end
