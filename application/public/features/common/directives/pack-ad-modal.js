'use strict';

/**
 * @ngdoc directive
 * @name packAdApp.directive:packAdPopUp
 * @description
 * # packAdPopUp
 */
angular.module('packAdApp')
  .directive('packAdPopUp', ['$uibModal', '$compile', '$log', function($modal, compile, log) {
    return {
      restrict: 'A',
      scope: {
        operational: '=',
        options: '='
      },
      link: function(scope, element, attrs) {
        /**
         * About directive scope variables
         * operational -> Decides type of modal (type: boolean, default: false).
         *                This variable will autamatically set based on options passed.
         *                true   - requires templateUrl to open modal. (Operational Pop Up)
         *                false  - requires a message and action options (Informative pop up with buttons)
         * options     -> There are two types of options that can be passed based on type of Modal
         *                Type 1 - {
         *                           message: String       - Require, Message to display in Modal
         *                           actionButtons: [
         *                            {
         *                              label: String      - Require, Lable to be displayed the button.
         *                              callBack: Function - Optional, Function to be called on clicking the button.
         *                              buttonType: String - Class to be added to button.
         *                            }
         *                           ]
         *                           onlyInfo: Boolean     - Decides if modal is only for information
         *                         }
         *                Type 2 - {
         *                          url: String            - Required, TemplateUrl of the html to be displayed in modal.
         *                          resolve: *             - Optional, any option that you want to be available in 
         *                                                   controller spectific to modal - ewtModalController.
         *                         }
         */

        scope.operational = scope.operational ? scope.operational : false;

        var ModalConstructor = function() {
          this.controller = 'packAdModalController';
          this.resolve = {
            resolved: function() {
              return {};
            }
          };
          this.animation = true;
          this.backdrop = 'static';
          this.keyboard = false;
          this.backdropClass = '';
          this.windowClass = 'dasdasdadasda';
          this.windowTopClass = 'windowTopClass';
          this.windowTemplateUrl = '';
          log.info('setting the default size of model to large');
          this.size = 'lg';
          this.openedClass = '';
        };

        var modalObject = {};

        scope.codeError = {
          message: [],
          onlyInfo: true
        };

        var checkOptions = function(options, keys) {
          var optionKeys = Object.keys(options);
          keys.forEach(function(value) {
            if (optionKeys.indexOf(value) === -1) {
              log.error('Options provided for popup doesnot contain the key - \'' + value + '\'');
            }
          });
        };

        var validateType = function(value, type, ref) {
          if (typeof(value) === type) {
            return true;
          } else {
            log.error('Expected a \'' + type + '\' for \'' + ref + '\' attribute in options');
          }
        };

        var validateOptions = function() {
          if (scope.operational) {
            // modal to be opened is operational. (Options should be type 2)
            checkOptions(scope.options, ['url', 'resolve']);
            if (scope.options.url) {
              validateType(scope.options.url, 'string', 'url');
            }
          } else {
            // modal to be opened is not operational. (Options should be type 1)
            scope.options.onlyInfo = scope.options.onlyInfo ? true : false;
            if (scope.options.onlyInfo) {
              checkOptions(scope.options, ['message']);
            } else {
              checkOptions(scope.options, ['message', 'actionButtons']);
            }
            if (scope.options.message) {
              validateType(scope.options.message, 'string', 'message');
            }
            if (scope.options.actionButtons && angular.isArray(scope.options.actionButtons)) {
              scope.options.actionButtons.forEach(function(button, index) {
                validateType(button.label, 'string', 'actionButtons[' + index + ']');
                if (button.callBack) {
                  validateType(button.callBack, 'function', 'actionButtons[' + index + ']');
                }
                button.buttonType = button.buttonType || 'secondary';
              });
            }
          }
        };

        var renderPopUp = function(renderObject, typeFlag) {
          modalObject = new ModalConstructor();
          scope.renderObject = {};
          scope.renderObject = angular.copy(renderObject);
          // If typeFlag is true then operational pop up will open up
          if (!typeFlag) {
            if (!(renderObject) || !(renderObject.message)) {
              // If we dont have anything to render, simply return back
              return;
            }

            var template = '<form>' +
              '<div class="modal-body">' +
              '<p style="font-size: 20px;text-align: center;font-weight: 500;">{{renderObject.message}}' +
              '</p>' +
              '</div>' +
              '<div class="modal-footer" style="text-align: center;border: none;">' +
              '<button ng-if="!renderObject.onlyInfo" ng-repeat="action in renderObject.actionButtons" ng-class="action.buttonType" ng-click="action.callBack();close($event)" ng-bind="action.label"></button>' +
              '<button ng-if="renderObject.onlyInfo" class="error" ng-click="close()" >Close</button>' +
              '</div>' +
              '</form>';
            modalObject.template = template;
          } else {
            modalObject.templateUrl = scope.renderObject.url;
            if (scope.renderObject.resolve) {
              modalObject.resolve.resolved = function() {
                // if the same reference is passed then then we can have a Circular Object.
                return angular.copy(scope.renderObject.resolve);
              };
            }
          }

          scope.renderObject.closeOn = scope.renderObject.closeOn || [];

          if (scope.renderObject.closeOn.indexOf('background') !== -1) {
            modalObject.backdrop = true;
          } else {
            modalObject.backdrop = 'static';
          }

          if (scope.renderObject.closeOn.indexOf('escape') !== -1) {
            modalObject.keyboard = true;
          } else {
            modalObject.keyboard = false;
          }

          var modalInstance = $modal.open(modalObject);
          modalInstance.result.then(function(response) {
            // Do nothing
          });

          modalInstance.opened.then(function(response) {
            // Do nothing
          });
          modalInstance.rendered.then(function(response) {
            if (!typeFlag) {
              scope.close = modalInstance.close;
              $('.modal-content[uib-modal-transclude]').children().remove();
              $('.modal-content[uib-modal-transclude]').append(compile(template)(scope));
            }
          });
        };

        var openPopUp = function(options) {
          scope.options = scope.options || {}
          if (options) {
            log.info('should refactor open pop up');
            log.info('Create a options constructor that returns options to be passed for every instance');
            // scope.options = angular.copy(options);
            if (options.url) {
              delete scope.options.message;
              delete scope.options.actionButtons;
              delete scope.options.onlyInfo;
            } else if (options.message) {
              delete scope.options.url;
              delete scope.options.resolve;
              options.onlyInfo = (options.actionButtons && options.actionButtons.length > 0) ? false : true;
            }
            delete scope.options.closeOn;
            scope.options = angular.extend({}, scope.options, options);
          } else {
            log.warn('Options to open modal are missing');
            return false;
          }

          /**
           * if the options passed doesnot have URL in it then set operational to true
           */
          scope.operational = (Object.keys(scope.options).indexOf('url') !== -1) ? true : false;

          /**
           * @ Validate if options passed are valid
           */
          validateOptions();

          renderPopUp(scope.options, scope.operational);
        };

        if (scope.options) {
          scope.options.open = openPopUp;
        } else {
          log.error('Directive pack-ad-pop-up should have a attribute \'options\' with a defined object');
        }
      }
    };
  }]);
