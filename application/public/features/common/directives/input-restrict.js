angular.module('packAdApp')
  .directive('paInputField', function() {
    return {
      restrict: 'A',
      scope: {},
      link: function(scope, element, attrs) {
        element.on('keypress', function(event) {
          var regex, key;
          if (!attrs.regxPattern) {
            return true;
          }
          regex = new RegExp(attrs.regxPattern);
          key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
          /**In firefox the keypress event includes the backspace,delete and other meta keys where as chrome ignores this.
           *Here we need to have the conditions for the meta keys to allow.
           *@ code 8 for backspace and 0 for delete and arrow keys.
           */
          var code = event.charCode === 0 ? event.which : event.charCode;
          if (!regex.test(key) && [8, 0].indexOf(code) === -1) {
            event.preventDefault();
            return false;
          }
        });
      }
    };
  });
