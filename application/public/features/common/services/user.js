/**
 * 
 *   Pack add User service
 *
 *   This file exports a class that can be used for all the operations
 *   on user
 *   
 */

angular.module('packAdApp')
  .factory('paUser', ['$resource', 'APISERVER', '$state', 'Upload', '$window', '$http', function(resource, apiRoute, state, uploader, window, http) {

    /**
     * url - default route for all the user specific operations
     */
    var url = apiRoute + 'users/:id';
    var uploaderUrl = apiRoute + 'upload/user/';
    var authHeaders = { 'authKey': window.sessionStorage.getItem('accessKey') } || {};

    /**
     * transformRequest - Method for transforming request object 
     *                    before sending to http 
     * @param  {Object} data - data sent to resource method
     * @return {Object}      data sent to http method
     */
    var transformRequest = function(request) {
      // operations on data being sent to http
      return angular.toJson(request);
    };

    /**
     * transformResponse - Method for transdorming the response 
     *                     recieved from http
     * @param  {Object} result - data recieved from http
     * @return {Object}        -  data being sent in the promise
     */
    var transformResponse = function(data) {
      // operations on response from http
      return angular.fromJson(data);
    };

    /**
     * paUser - Pack ad user object 
     *          This object is the class that we expose as a dependency
     * @type {Object}
     */
    var paUser = resource(url, { 'id': '@_id' }, {
      save: {
        method: 'POST',
        isArray: false,
        transformRequest: transformRequest,
        transformResponse: transformResponse
      },
      query: {
        method: 'GET',
        params: {},
        isArray: true
      },
      get: {
        method: 'GET',
        isArray: false,
        transformResponse: transformResponse
      },
      update: {
        method: 'POST',
        isArray: false,
        headers: authHeaders,
        transformRequest: transformRequest,
        transformResponse: transformResponse
      },
      logIn: {
        params: { 'id': 'logIn' },
        method: 'POST',
        isArray: false
      },
      logOut: {
        params: { 'id': 'logout' },
        method: 'GET',
        isArray: false
      },
      getVarified: {
        params: { 'id': 'sendEmailVerification' },
        headers: authHeaders,
        method: 'POST',
        isArray: false
      }
    });

    /**
     * makeSave - Adding methods to the class that will be accessable
     *            when we create a new user
     */
    paUser.prototype.makeSave = function(image) {
      var operation;
      /**
       * userClient - maintaining current instance
       *              as it will be used after first DB call
       */
      var userClient = this;
      if (this._id) {
        operation = this.$update();
      } else {
        operation = this.$save();
      }

      /**
       * operation is a promise.
       *
       * .then contains two functions
       * 1st is success handler 
       * 2nd is error handler
       */
      operation.then(function(data) {
        /**
         * If user has uploaded the image then save the uploaded image
         * else move to home page
         */
        if (image) {
          userClient.uploadProfile(uploaderUrl + data.user._id, image, function(resp) {
            window.sessionStorage.setItem('accessKey', data.key);
            window.sessionStorage.setItem('id', data.user._id);
            if(data.user.role === "Business") {
              state.go('home', { 'id': data.user._id });
            }
            else {
              state.go('consumerHome', { 'id': data.user._id });
            }
          });
        } else {
          /**
           * if user did not upload image move to home page
           */
          window.sessionStorage.setItem('accessKey', data.key);
          window.sessionStorage.setItem('id', data.user._id);
          state.go('home', { 'id': data.user._id });
        }
      }, function(err) {
        // We could show model window here
        console.log(err);
      });
    };

    /**
     * uploadProfile - uploads profile pic of the user
     * @param  {String}    url      - URL to be hit for uploading file
     * @param  {Object}    file     - file to be uploaded
     * @param  {Function1} callBack - call back after upload is successfull
     */
    paUser.prototype.uploadProfile = function(url, file, callBack) {
      uploader.upload({
        url: url,
        data: { 'file': file }
      }).then(function(resp) {
        callBack(resp);
      });
    };


    /**
     * checkExistingEmail - Method to check if user is already registered in application
     * @param  {Function} callBack - call back function to be called after success
     */
    paUser.prototype.checkExistingEmail = function(callBack) {
      /**
       * email - stores email from the user instance
       * @type {String}
       */
      var email = this.email;

      http({
        method: 'POST',
        url: apiRoute + 'users/checkEmailExist',
        data: { 'email': email }
      }).then(function(response) {
        callBack(response.data);
      });
    };

    return paUser;
  }]);
