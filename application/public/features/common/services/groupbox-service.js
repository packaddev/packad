angular.module('packAdApp')
  .factory('GroupBoxService', function($http) {
    var groupBoxService = {};

    groupBoxService.connect = function(groupBoxDetails) {
      return $http
        .post('/api/groupbox/connect', groupBoxDetails)
        .then(function(res) {
          console.log(res.data);
          var groupbox = {
            id: res.data.id,
            success: res.data.success
          };
          return groupbox;
        }, function(error) {
          console.log('Error in Group box save: ', error);
          var groupbox = {
            id: -1,
            success: false
          }
        })
    };

    groupBoxService.disconnect = function(groupBoxDetails) {
      return $http
        .post('/api/groupbox/disconnect', groupBoxDetails)
        .then(function(res) {
          console.log(res.data);
          var groupbox = {
            id: res.data.id,
            success: res.data.success
          };
          return groupbox;
        });
    };

    groupBoxService.getGroupBoxes = function(userId) {
      return $http
        .get('/api/groupbox/getAll/' + userId);
    };


    return groupBoxService;
  });
