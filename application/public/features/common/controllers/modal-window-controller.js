'use strict';

/**
 * @ngdoc function
 * @name packAdApp.controller:packAdModalController
 * @description
 * # packAdModalController
 */
angular.module('packAdApp')
  .controller('packAdModalController', ['$scope', '$log', '$uibModalInstance', 'resolved', 'ad', 'commonDataService',
    function(scope, log, modalInstance, resolved, adClass, commonDataService) {

      scope.close = function(value) {
        modalInstance.close(value);
      };


      if (resolved.purpose === 'adCreation') {
        /**
         * ad - new ad object
         * @type {adClass}
         */
        scope.ad = new adClass(resolved.details);

        scope.group = resolved.details;

        scope.data = {
          sizes: ['S', 'M', 'L', 'XL', '1X', '2X', '3X']
        }
        commonDataService.getCountries()
          .success(function(response) {
            scope.data.countries = response;
          })
          .error(function(error) {
            console.log('Unable to load country data: ' + error.message);
          });

        commonDataService.getStates()
          .success(function(response) {
            scope.data.states = response;
          })
          .error(function(error) {
            console.log('Unable to load states data: ' + error.message);
          });


        /**
         * creationFlow - Variable that holds the flow..
         *                the strings are the keys for their respective viees
         *                These SHOULD follow the order in which ad is created!!
         * @type {[String]}
         */
        var creationFlow = ['initial', 'showGroup', 'adType', 'supportUpload', 'shipment', 'refundPolicy', 'semiAcknowledgement', 'composeAd', 'preview', 'dealTime', 'discount', 'workFlow', 'adVisibility', 'finalEdit', 'adCreated'];

        /**
         * currentView - Name of the current view.
         * @type {String}
         */
        var currentView = 'initial';

        /**
         * adViews - Nested objecct that contains details of all the views in the 
         *           process of creating a Ad.
         * @type {Object}
         */
        var adViews = {
          'initial': {
            source: 'features/ad/partials/start-page.html',
            buttons: [{
              label: 'Go',
              type: 'btn-primary',
              action: function() {
                scope.nextPage();
              },
              disabled: function() {
                return false;
              }
            }]
          },
          'showGroup': {
            source: 'features/ad/partials/confirm-group.html',
            buttons: [{
              label: 'Confirm & Continue',
              type: 'btn-primary',
              action: function() {
                scope.nextPage();
              },
              disabled: function() {
                return false;
              }
            }]
          },
          'changeGroup': {
            source: 'features/ad/partials/change-group.html',
            buttons: [{
              label: 'Cancel change',
              type: 'btn-danger',
              action: function() {
                scope.goToView('showGroup');
              },
              disabled: function() {
                return false;
              }
            }, {
              label: 'Confirm & Continue',
              type: 'btn-primary',
              action: function() {
                scope.nextPage();
              },
              disabled: function() {
                return false;
              }
            }],
            nextView: 'showGroup'
          },
          'adType': {
            source: 'features/ad/partials/select-ad-type.html',
            buttons: [{
              label: 'Next',
              type: 'btn-primary',
              action: function() {
                scope.ad.serviceDetails = {};
                if (scope.ad.service === 'product') {
                  scope.goToView('productDetails');
                }
                if (scope.ad.service === 'service') {
                  scope.goToView('serviceDetails');
                }
              },
              disabled: function() {
                return !scope.ad.service;
              }
            }]
          },
          'productDetails': {
            source: 'features/ad/partials/product-details.html',
            buttons: [{
              label: 'Back',
              type: 'btn-primary',
              action: function() {
                scope.goToView('adType');
              },
              disabled: function() {
                return false;
              }
            }, {
              label: 'Next',
              type: 'btn-primary',
              action: function() {
                scope.nextPage();
              },
              disabled: function(form) {
                if (scope.ad.serviceDetails.itemDetails) {
                  return form.$invalid || scope.ad.serviceDetails.itemDetails === 0;
                } else {
                  return true;
                }
              }
            }],
            nextView: 'supportUpload'
          },
          'serviceDetails': {
            source: 'features/ad/partials/service-details.html',
            buttons: [{
              label: 'Back',
              type: 'btn-primary',
              action: function() {
                scope.goToView('adType');
              },
              disabled: function() {
                return false;
              }
            }, {
              label: 'Next',
              type: 'btn-primary',
              action: function() {
                scope.nextPage();
              },
              disabled: function(form) {
                if (scope.ad.serviceDetails.itemDetails) {
                  return form.$invalid || scope.ad.serviceDetails.itemDetails === 0;
                } else {
                  return true;
                }
              }
            }],
            nextView: 'supportUpload'
          },
          'supportUpload': {
            source: 'features/ad/partials/upload-details.html',
            buttons: [{
              label: 'Back',
              type: 'btn-primary',
              action: function() {
                if (scope.ad.service === 'product') {
                  scope.goToView('productDetails');
                }
                if (scope.ad.service === 'service') {
                  scope.goToView('serviceDetails');
                }
              },
              disabled: function() {
                return false;
              }
            }, {
              label: 'Next',
              type: 'btn-primary',
              action: function() {
                scope.nextPage();
              },
              disabled: function() {
                return !scope.ad.primaryImg;
              }
            }],
            nextView: 'shipment'
          },
          'shipment': {
            source: 'features/ad/partials/shiping-details.html',
            buttons: [{
              label: 'Back',
              type: 'btn-primary',
              action: function() {
                scope.goToView('supportUpload');
              },
              disabled: function() {
                return false;
              }
            }, {
              label: 'Next',
              type: 'btn-primary',
              action: function() {
                scope.nextPage();
              },
              disabled: function(form) {
                return form.$invalid;
              }
            }]
          },
          'refundPolicy': {
            source: 'features/ad/partials/refund-policy.html',
            buttons: [{
              label: 'Back',
              type: 'btn-primary',
              action: function() {
                scope.goToView('shipment');
              },
              disabled: function() {
                return false;
              }
            }, {
              label: 'Next',
              type: 'btn-primary',
              action: function() {
                scope.nextPage();
              },
              disabled: function(form) {
                return form.$invalid;
              }
            }]
          },
          'semiAcknowledgement': {
            source: 'features/ad/partials/semi-acknowledgement.html',
            buttons: [{
              label: 'Back',
              type: 'btn-primary',
              action: function() {
                scope.goToView('refundPolicy');
              },
              disabled: function() {
                return false;
              }
            }, {
              label: 'Next',
              type: 'btn-primary',
              action: function() {
                scope.nextPage();
              },
              disabled: function() {
                return false;
              }
            }]
          },
          'preview': {
            source: 'features/ad/partials/ad-preview.html',
            buttons: [{
              label: 'Continue',
              type: 'btn-primary',
              action: function() {
                if (scope.previewedFrom) {
                  scope.goToView(scope.previewedFrom);
                } else {
                  scope.nextPage();
                }
              },
              disabled: function() {
                return false;
              }
            }]
          },
          'composeAd': {
            source: 'features/ad/partials/ad-compose.html',
            buttons: [{
              label: 'Back',
              type: 'btn-primary',
              action: function() {
                scope.goToView('semiAcknowledgement');
              },
              disabled: function() {
                return false;
              }
            }, {
              label: 'Next',
              type: 'btn-primary',
              action: function() {

                scope.nextPage();
              },
              disabled: function(form) {
                return form.$invalid;
              }
            }]
          },
          'dealTime': {
            source: 'features/ad/partials/deal-start.html',
            buttons: [{
              label: 'Back',
              type: 'btn-primary',
              action: function() {
                scope.goToView('composeAd');
              },
              disabled: function() {
                return false;
              }
            }, {
              label: 'Next',
              type: 'btn-primary',
              action: function() {
                scope.nextPage();
              },
              disabled: function() {
                return false;
              }
            }]
          },
          'discount': {
            source: 'features/ad/partials/discount.html',
            buttons: [{
              label: 'Back',
              type: 'btn-primary',
              action: function() {
                scope.goToView('dealTime');
              },
              disabled: function() {
                return false;
              }
            }, {
              label: 'Next',
              type: 'btn-primary',
              action: function() {
                scope.nextPage();
              },
              disabled: function(form) {
                return form.$invalid;
              }
            }]
          },
          'workFlow': {
            source: 'features/ad/partials/work-flow.html',
            buttons: [{
              label: 'Back',
              type: 'btn-primary',
              action: function() {
                scope.goToView('discount');
              },
              disabled: function() {
                return false;
              }
            }, {
              label: 'Next',
              type: 'btn-primary',
              action: function() {
                scope.nextPage();
              },
              disabled: function() {
                return false;
              }
            }]
          },
          'adVisibility': {
            source: 'features/ad/partials/visibility.html',
            buttons: [{
              label: 'Back',
              type: 'btn-primary',
              action: function() {
                scope.goToView('workFlow');
              },
              disabled: function() {
                return false;
              }
            }, {
              label: 'Next',
              type: 'btn-primary',
              action: function() {
                scope.nextPage();
              },
              disabled: function(form) {
                return form.$invalid;
              }
            }]
          },
          'finalEdit': {
            source: 'features/ad/partials/edit-preview.html',
            buttons: [{
              label: 'Save & Post Later',
              type: 'btn-primary',
              action: function() {
                // save Ad here AND CLOSE
                scope.ad.status = 'saved';
                scope.ad.makeSave();
                scope.close();
              },
              disabled: function() {
                return false;
              }
            }, {
              label: 'Finish & Post PackAd to commercial group',
              type: 'btn-primary',
              action: function() {
                // save Ad here
                scope.ad.status = 'posted';
                scope.ad.makeSave();
                scope.nextPage();
              },
              disabled: function() {
                return false;
              }
            }]
          },
          'adCreated': {
            source: 'features/ad/partials/ad-created-acknowledged.html'
          }
        };

        /**
         * Setting the default view.
         */
        scope.view = adViews[currentView];
        
        /**
         * nextPage - used to move to next view.
         *            Takes in the cureent view.
         *            If the view is found in the creationFlow,
         *            that means its a main view and will switch to its next move
         *            If not found then current view is a subview.
         *            If we have a subview then we need to have a key in view Object
         *            that has reference to its next main view.
         */
        scope.nextPage = function(forPreview) {

          if (scope.reference) {
            currentView = scope.reference;
          } else {
            /**
             * chceking if the currently displayed view is in the Flow
             */
            var stage = creationFlow.indexOf(currentView);
            if (stage !== -1) {
              currentView = creationFlow[stage + 1];
            } else {
              currentView = scope.view.nextView;
            }
          }
          /**
           * set the next view based
           */
          scope.view = adViews[currentView];
          delete scope.reference;

          if (!forPreview) {
            /**
             * Removing preview Reference
             */
            delete scope.previewedFrom;
          }
        };

        /**
         * goToView - method used to diectly open a view irrespective of 
         *            ad creation flow.
         *            This has to be used only to go out of ad creation mail flow
         *            Ex:- Changing groups is not part of main creation flow.
         *                 It is a sub part, hence use this method
         * @param  {String} viewName - name of the view to be switched to.
         */
        scope.goToView = function(viewName, forPreview, fromFinalPreview) {
          /**
           * allViews - Array that holds all the views irrespective of if they are main or not
           * @type {[String]}
           */
          var allViews = Object.keys(adViews);

          var viewIndex = allViews.indexOf(viewName);

          if (fromFinalPreview) {
            scope.reference = currentView;
          }

          /**
           * Changing the view only if the view mentioned as argument
           * is present in all the views defined.
           */
          if (viewIndex !== -1) {
            currentView = viewName;
            scope.view = adViews[viewName];
          } else {
            log.warn('You are tying to switch to unkonwn view. Check the name you mentioned as view');
          }

          if (!forPreview) {
            /**
             * Removing preview Reference
             */
            delete scope.previewedFrom;
          }
        };

        scope.previewAd = function() {
          scope.previewedFrom = currentView;
          scope.goToView('preview', true);
        };

        scope.editPreview = function() {
          scope.previewedFrom = currentView;
          scope.goToView('editPreview', true);
        }

        scope.addDetail = function(newDetails) {
          if (scope.newDetailValid(newDetails)) {
            scope.ad.serviceDetails.itemDetails = scope.ad.serviceDetails.itemDetails || [];
            scope.ad.serviceDetails.itemDetails.push(newDetails);
          } else {
            log.warn('Do not debug the code');
          }
        };

        scope.removeDetail = function(index) {
          scope.ad.serviceDetails.itemDetails.splice(index, 1);
        };

        scope.newDetailValid = function(details) {
          if (details && details.size && details.quantity) {
            return scope.ad.service === 'product' ? (details.color ? true : false) : true;
            // return true;
          } else {
            return false;
          }
        };

        scope.removePrimaryImage = function() {
          scope.ad.primaryImg = {};
        }

        scope.removeSecondaryImage = function(index) {
          scope.ad.secondaryImages.splice(index, 1);
        }

        scope.calculateCost = function() {
          if (scope.ad.price && scope.ad.discount) {
            scope.ad.effectivePrice = (((100 - scope.ad.discount) / 100) * scope.ad.price).toFixed(2);
          }
        };

        scope.openAdDatePicker = function() {
          scope.ad.datePickeropened = true;
        };

        scope.adDatesOptions = {
          // dateDisabled: disabled,
          formatYear: 'yy',
          maxDate: new Date(2020, 5, 22),
          minDate: new Date(),
          startingDay: 1
        };

        scope.cancel = function() {
          scope.exiting = true;
        };

        scope.stayOnPage = function() {
          scope.exiting = false;
        };
      }
    }
  ]);
