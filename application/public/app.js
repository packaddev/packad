/**
 *
 *  Main module that triggers the application.
 *
 *  Custom modules that are injected as dependencies are
 *   # pa-login
 * 
 */

angular.module('packAdApp', [
  'ivh.treeview',
  'ui.router',
  'ngResource',
  'ngAnimate',
  'ui.bootstrap',
  'ngMaterial',
  'smart-table',
  // custopm modules
  'signUp'
]).config(function($stateProvider, $urlRouterProvider, $locationProvider) {

  /**
   * Setting the default route to '/'
   */
  $urlRouterProvider.otherwise('/');
  $stateProvider
  /**
   * launch - This is the main state.
   *          Gets loaded on launching the application.
   */
    .state('launch', {
      url: '/',
      templateUrl: '../features/launch/launch.html',
      controller: 'LaunchController',
      controllerAs: 'lc'
    })
    /**
     * signup - This is an abstract state that gets loaded for sign up
     *          we pass new user as he is signing up for the application
     */
    .state('signup', {
      //abstract: true,
      url: '/signUp',
      templateUrl: '../features/signup/signup.html',
      resolve: {
        user: function(paUser) {

          window.sessionStorage.removeItem('id');

          /**
           * Returning a new user irrespective of his role 
           */
          return new paUser({
            /**
             * we can add default key value pairs here
             */
            'email': '',
            'password': '',
            'companyName': '',
            'businessCategory': {},
            'country': {},
            'state': {},
            'website': '',
            'role': '',
            'firstName': '',
            'lastName': '',
            'middleName': '',
            'fulltName': ''
          });
        }
      },
      controller: 'SignUpController',
      controllerAs: 'signUpCt'
    })
    /**
     * home - this state gets loaded once the user validation is successfull
     */
    .state('home', {
      url: '/home/business',
      params: { 'id': null },
      templateUrl: '../features/home/home.html',
      controller: 'HomeController',
      controllerAs: 'hc',
      resolve: {
        user: function(paUser, $stateParams, $state) {
          var uid = window.sessionStorage.getItem('id');
          if (uid === null) {
            $state.go('launch');
          }
          return paUser.get({
            id: uid
          }).$promise.then(function(response) {
            return response;
          }, function(error) {
            return error;
          });
        },
        businessCategories: function(commonDataService) {
          return commonDataService.getBusinessCategories()
            .success(function(response) {
              return response;
            })
            .error(function(error) {
              console.log('Unable to load business categories data: ' + error.message);
            });
        },
        businessSubCategories: function(commonDataService) {
          return commonDataService.getBusinessSubCategories()
            .success(function(response) {
              return response;
            })
            .error(function(error) {
              console.log('Unable to load business subcategories data: ' + error.message);
            });
        },
        groupBoxDetailsList: function(GroupBoxService, $stateParams) {
          var uid = window.sessionStorage.getItem('id');
          return GroupBoxService.getGroupBoxes(uid)
            .success(function(response) {
              return response;
            })
            .error(function(error) {
              console.log('Unable to GroupBox details list: ' + error.message);
            });
        }
      }
    })
    /**
    * Consumer home - this state gets loaded once the user validation is successfull
    */
    .state('consumerHome', {
      url: '/home/consumer',
      params: { 'id': null },
      templateUrl: '../features/consumerHome/consumer-home.html',
      controller: 'ConsumerHomeController',
      controllerAs: 'chc',
      resolve: {
        user: function(paUser, $stateParams, $state) {
          var uid = window.sessionStorage.getItem('id');
          if (uid === null) {
            $state.go('launch');
          }
          return paUser.get({
            id: uid
          }).$promise.then(function(response) {
            return response;
          }, function(error) {
            return error;
          });
        },
        businessCategories: function(commonDataService) {
          return commonDataService.getBusinessCategories()
            .success(function(response) {
              return response;
            })
            .error(function(error) {
              console.log('Unable to load business categories data: ' + error.message);
            });
        },
        businessSubCategories: function(commonDataService) {
          return commonDataService.getBusinessSubCategories()
            .success(function(response) {
              return response;
            })
            .error(function(error) {
              console.log('Unable to load business subcategories data: ' + error.message);
            });
        },
        groupBoxDetailsList: function(GroupBoxService, $stateParams) {
          var uid = window.sessionStorage.getItem('id');
          return GroupBoxService.getGroupBoxes(uid)
            .success(function(response) {
              return response;
            })
            .error(function(error) {
              console.log('Unable to GroupBox details list: ' + error.message);
            });
        }
      }
    });

  /**
   * Removing # from url.
   */
  $locationProvider.html5Mode({
    enabled: false,
    requireBase: false
  });
});

/**
 * empty anchor tags href=# issue
 */
angular.module('packAdApp').directive('a', function() {
  return {
    restrict: 'E',
    link: function(scope, elem, attrs) {
      if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
        elem.on('click', function(e) {
          e.preventDefault();
        });
      }
    }
  };
});
