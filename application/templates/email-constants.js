var config = require('../../config/config.js');
var mailConstants = function() {
  this.confirmationBody = 'Hi {{Name}} <br/><br/>Thanks for Joining PackAd! Please click the button below to confirm your registration. Without verification you will not be able to fully use PackAd.<br/><a href="{{BaseUrl}}/api/users/EmailConfirmation/{{VerStr}}">Verify</a>';

  this.confirmationSubject = 'Please Confirm your PackAd user account'

  this.replaceKeys = function(str, keys) {
    str = str.replace('{{Name}}', keys.Name);
    str = str.replace('{{VerStr}}', keys.VerStr);
    str = str.replace('{{BaseUrl}}', config.emailBaseUrl)

    return str;
  }

}

module.exports = mailConstants;
