'use strict';

var express = require('express');
var http = require('http');
var kraken = require('kraken-js');
var appUtils = require('./application/lib/app-utils.js');
var router = require('./application/routes/api.js');
var db = require('./application/lib/db.js');
var config = require('config').application;

/**
 * dbConfig - Holds all the information related to MongoDB server
 * @type {Object}
 */
var dbConfig = config.mongo;
/**
 * dbUser - User name for the DB
 * @type {String}
 */
var dbUser = dbConfig.auth.user;
/**
 * dbPass - Password specific to User
 * @type {String}
 */
var dbPass = dbConfig.auth.pass;

/**
 * mongoAuth - Mongo authentication string 
 *             Will be a part of Mongo Url if user and password are present
 * @type {String}
 */
var mongoAuth = '';
/**
 * hostString - Host address (including port) for mongoDB
 * @type {String}
 */
var hostString = '';
/**
 * mongoURL - Conplete MongoDB url
 *            MongoAuth + hostString
 * @type {String}
 */
var mongoURL = '';

/**
 * options - These are the options sent in as kraken configuration
 */
var options;
/**
 * app - Express application
 */
var app;


/**
 * Form mongoAuth if user password exists
 */
if (dbConfig.auth.user && dbConfig.auth.pass) {
  mongoAuth = dbUser + ':' + dbPass + '@';
}

/**
 * creating host string based on the instances in that particular environment
 */
for (var indx = 0; indx < dbConfig.instances.length; indx++) {
  hostString = indx ? hostString + ',' : hostString;
  hostString += dbConfig.instances[indx].host + ':' + dbConfig.instances[indx].port;
}

/**
 * Creating the complete mongoDB url
 * @type {String}
 */
mongoURL = 'mongodb://' + mongoAuth + hostString + '/' + dbConfig.db;

/**
 * Creating express application
 */
app = express();

/*
 * Create and configure application. Also exports application instance for use by tests.
 * See https://github.com/krakenjs/kraken-js#options for additional configuration options.
 */
options = {
  onconfig: function(config, next) {
    /*
     * Add any additional config setup or overrides here. `config` is an initialized
     * `confit` (https://github.com/krakenjs/confit/) configuration object.
     */
    appUtils.setConfig(config);
    next(null, config);
  }
};

app.use(kraken(options));

app.on('start', function() {
  app.use('/', router);
  console.log('Environment: %s', app.kraken.get('env:env'));
});


/**
 * Connecting to DB
 * @param  {mongodb server url}
 */
db.connect(mongoURL, dbConfig.options || {}, function(err, success) {
  if (err) {
    console.log('\n DB connect :: Fail');
    console.log(dbError);
    return dbError;
  }
  /*
   * Create and start HTTP server.
   */
  var server = http.createServer(app);
  server.listen(process.env.PORT || config.http.port, function(err) {
    if (err) {
      console.log('error in listening to the port mentioned');
    } else {
      console.log('Server listening on http://localhost:%d', this.address().port);
    }
  });
});
